# CMake OM15080-K32W Hello World example
This Hello World example runs on the OM15080-K32W eval board from NXP (https://www.nxp.com/products/wireless/bluetooth-low-energy/om15080-k32w-k32w-usb-dongle-for-bluetooth-le-zigbee-and-thread-networks:OM15080-K32W)

# Prerequisites
To build and run this example on the OM15080-K32W you will need
  
  - GNU ARM Embedded toolchain from https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm
  - CMake > 3.0
  - Ninja build
  - DK6Programmer from NXP MCUXpresso SDK. Create a SDK for your target platform via the SDK builder at https://mcuxpresso.nxp.com/en/welcome

From the NXP MCUXpresso SDK you will need to install the DK6Programmer tool under `SDK_2.6.1_K32W061\tools\JN-SW-4407-DK6-Flash-Programmer`. This tool is currently only available for Windows.
Add the path to the toolchain bin folder (e.g. `C:\Program Files (x86)\GNU Arm Embedded Toolchain\9 2020-q2-update\bin`) and DK6Programmer (e.g. `C:\NXP\DK6ProgroductionFlashProgrammer/DK6Programmer`) to your PATH environment variable.

The files under SDK included in this repository are part of the ***NXP MCUXpresso SDK*** which you can retrieve from NXP at https://mcuxpresso.nxp.com/en/welcome

# Build Example
The example can be build via CMake

    ❯ mkdir build
    ❯ cd build 
    ❯ cmake -GNinja -DCMAKE_TOOLCHAIN_FILE=../toolchain/armgcc.cmake ..
    -- The C compiler identification is GNU 9.3.1
    -- The ASM compiler identification is GNU
    -- Found assembler: /opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/arm-none-eabi-gcc
    -- Detecting C compiler ABI info
    -- Detecting C compiler ABI info - done
    -- Check for working C compiler: /opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/    arm-none-eabi-gcc - skipped
    -- Detecting C compile features
    -- Detecting C compile features - done
    -- Configuring done
    -- Generating done
    -- Build files have been written to: <your-source-path>/build
    ❯ ninja

This will generate a binary (helloWorld.bin) file for flashing to your stick.

# Flashing Hello World example
Plugin the OM15080-K32W and make sure that you have drivers for FTDI Virtual Comport Drivers installed (https://www.ftdichip.com/Drivers/VCP.htm). Now the Stick should show up in your device mangager with a specific COM Port. You can check if DK6Programmer can see your device via

    $ DK6Programmer.exe -l
    Available connections:
    COM3

If it shows at least one COM Port it found the Virtual Com Port of the OM15080-K32W. First you should erase the flash of your board via

    $ DK6Programmer.exe -s COM3 -e
    Your terminal window is too small for ncurses mode. The minimum size is 62x22
    COM3: Connected at 115200
    COM3: Detected K32W061 with MAC address 00:15:8D:00:03:D8:49:0A
    COM3: Selected memory: FLASH
    COM3: Erasing FLASH
    COM3: Partial erase required on memory FLASH, addr=0x00000000, length=646656
    COM3: Erasing FLASH
    COM3: 0
    COM3: Completed 

Now you can flash the binary via

    $ DK6Programmer.exe -s COM3 -p helloWorld.bin
    Your terminal window is too small for ncurses mode. The minimum size is 62x22
    COM3: Connected at 115200
    COM3: Detected K32W061 with MAC address 00:15:8D:00:03:D8:49:0A
    COM3: Selected memory: FLASH
    COM3: Programming FLASH at 0x0
    COM3: Partial erase required on memory FLASH, addr=0x00000000, length=9920
    COM3: The area to erase is not an exact multiple of the erase block size. Erase data from 0x00000000 to 0x00002800?

    Y/N
    COM3: Erasing FLASH
    COM3: 0
    COM3: Completed
    COM3: Programming FLASH
    COM3: 0
    COM3: Programming FLASH
    COM3: 0
    COM3: Programming FLASH
    COM3: 5
    COM3: Programming FLASH
    COM3: 10
    COM3: Programming FLASH
    COM3: 15
    COM3: Programming FLASH
    COM3: 20
    COM3: Programming FLASH
    COM3: 25
    COM3: Programming FLASH
    COM3: 30
    COM3: Programming FLASH
    COM3: 36
    COM3: Programming FLASH
    COM3: 41
    COM3: Programming FLASH
    COM3: 46
    COM3: Programming FLASH
    COM3: 51
    COM3: Programming FLASH
    COM3: 56
    COM3: Programming FLASH
    COM3: 61
    COM3: Programming FLASH
    COM3: 67
    COM3: Programming FLASH
    COM3: 72
    COM3: Programming FLASH
    COM3: 77
    COM3: Programming FLASH
    COM3: 82
    COM3: Programming FLASH
    COM3: 87
    COM3: Programming FLASH
    COM3: 92
    COM3: Programming FLASH
    COM3: 98
    COM3: Programming FLASH
    COM3: 100
    COM3: Memory programmed successfully

and after flashing you can access the COM Port (e.g. via puty) and see a "Hello World" if you reset the device.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This Documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
