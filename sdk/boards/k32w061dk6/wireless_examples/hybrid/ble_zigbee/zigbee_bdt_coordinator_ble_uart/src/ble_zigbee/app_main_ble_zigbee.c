/*! *********************************************************************************
* Copyright (c) 2014, Freescale Semiconductor, Inc.
* Copyright 2016-2019 NXP
*
* \file
*
* This is a source file for the main application Zigbee + BLE.
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

/************************************************************************************
*************************************************************************************
* Include
*************************************************************************************
************************************************************************************/

/* Framework include */
#include "fsl_os_abstraction.h"
#include "MemManager.h"
#include "TimersManager.h"
#include "RNG_Interface.h"
#include "Messaging.h"
#include "Flash_Adapter.h"
#include "SecLib.h"
#include "Panic.h"
#include "LED.h"
#include "Keyboard.h"
#include "SerialManager.h"
#include "PDM.h"
#include "OtaSupport.h"
#include "NVM_Interface.h"

/* KSDK */
#include "board.h"

/* zigbee includes */
#include "app_main.h"
#include "app_coordinator.h"
#include "dbg.h"
#include "app_buttons.h"
#include "app_leds.h"
#include "zigbee_config.h"
#include "ZTimer.h"
#include "bdb_start.h"

/* ble includes */
#include "gatt_interface.h"
#include "gatt_server_interface.h"
#include "gatt_client_interface.h"
#include "gap_interface.h"
#include "ApplMain.h"
#include "ble_init.h"
#include "controller_interface.h"

/* dual mode include */
#include "app_dual_mode_switch.h"

/* Selective OTA */
#if defined (SOTA_ENABLED)
#include "blob_manager_app.h"
#include "blob_utils.h"
#endif

/************************************************************************************
*************************************************************************************
* Private Macro
*************************************************************************************
************************************************************************************/
#ifdef APP_SERIAL_LOGS_ENABLED
#define APP_SERIAL_PRINT(string) Serial_Print(gAppSerMgrIf, string, gAllowToBlock_d);
#define APP_SERIAL_PRINT_HEX(value, nbBytes) Serial_PrintHex(gAppSerMgrIf, value , nbBytes, gPrtHexNoFormat_c);
#else
#define APP_SERIAL_PRINT(...)
#define APP_SERIAL_PRINT_HEX(...)
#endif

#ifndef APP_DUAL_MODE_DEBUG
#define APP_DUAL_MODE_DEBUG FALSE
#endif

/************************************************************************************
*************************************************************************************
* Private definitions
*************************************************************************************
************************************************************************************/
typedef enum {
    eZigbeeBLEMode = eConMode1,
} eDualModeZbBle;

/************************************************************************************
*************************************************************************************
* Private prototypes
*************************************************************************************
************************************************************************************/

#if !defined (SOTA_ENABLED)
#if !gUseHciTransportDownward_d
static void BLE_SignalFromISRCallback(void);
#endif
#endif /* SOTA_ENABLED */

static void App_InitBle();
static void App_InitZigbee();
static void dm_switch_RunZigbeeTasks();
static void dm_switch_start(void);

#if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0)
static void App_KeyboardCallBack(uint8_t events);
#endif

/************************************************************************************
*************************************************************************************
* Private memory declarations
*************************************************************************************
************************************************************************************/
static const sConModeFunctions bleZigbeeConSwitchFunctions[]={
    /* ConMode0 is the zigbee/BLE mode 
     */
    {
        .dm_switch_mode = NULL,
        .dm_switch_start = dm_switch_start,
        .dm_switch_set_sleeping_status = NULL,
        .dm_reset_ota_state_machine = NULL,
    }
};

static uint8_t platformInitialized = 0;
static uint8_t app_running_con_mode = eZigbeeBLEMode;

#ifdef APP_SERIAL_LOGS_ENABLED
static uint8_t gAppSerMgrIf;
#endif

/************************************************************************************
*************************************************************************************
* Exported functions/variables
*************************************************************************************
************************************************************************************/

/* BLE Application input queues */
extern anchor_t mHostAppInputQueue;
extern anchor_t mAppCbInputQueue;
/* Zigbee queue */
extern tszQueue APP_msgAppEvents;

/* BLE application extern function */
extern osaStatus_t AppIdle_TaskInit(void);
extern void App_Thread (uint32_t param);
extern void App_GenericCallback (gapGenericEvent_t* pGenericEvent);
extern void BleApp_Init(void);
extern void BleApp_HandleKeys(key_event_t events);
extern void BleApp_Start(void);
extern void App_SecLibMultCallback(computeDhKeyParam_t *pData);

/* Shell extern functions */
extern void app_shell_init(void);

/* Zigbee extern function */
extern uint8* ZPS_pu8AplZdoGetVsOUI(void);

#if !defined (SOTA_ENABLED)
#if !gUseHciTransportDownward_d
extern void (*pfBLE_SignalFromISR)(void);
#endif /* gUseHciTransportDownward_d */
#endif /* SOTA_ENABLED */

/* BLE app event */
osaEventId_t  mAppEvent;

/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/

/*! *********************************************************************************
* \brief  This is the first task created by the OS. This task will initialize
*         the system
*
* \param[in]  param
*
********************************************************************************** */
void main_task(uint32_t param)
{
    if (!platformInitialized)
    {
        platformInitialized = 1;

        BOARD_SetFaultBehaviour();

        /* Framework init */
        MEM_Init();
        TMR_Init();
        LED_Init();

        /* Cryptographic and RNG hardware initialization */
        SecLib_Init();
        SecLib_SetExternalMultiplicationCb(App_SecLibMultCallback);
        /* RNG software initialization and PRNG initial seeding (from hardware) */
        RNG_Init();
        RNG_SetPseudoRandomNoSeed(NULL);

#if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0)
        KBD_Init(App_KeyboardCallBack);
#endif
        /* Init the flash driver */
        NV_Init();
        /* PDM init */
        PDM_Init();

        /* Init ADC */
        BOARD_InitAdc();

#ifdef APP_SERIAL_LOGS_ENABLED
        /* UI */
        Serial_InitManager();

        /* Register Serial Manager interface */
        Serial_InitInterface(&gAppSerMgrIf, APP_SERIAL_INTERFACE_TYPE, APP_SERIAL_INTERFACE_INSTANCE);

        Serial_SetBaudRate(gAppSerMgrIf, BOARD_DEBUG_UART_BAUDRATE);
#endif


        /* Init the Idle task */
        AppIdle_TaskInit();

        /* Create application event */
        mAppEvent = OSA_EventCreate(TRUE);
        if( NULL == mAppEvent )
        {
            panic(0,0,0,0);
            return;
        }

#if defined (SOTA_ENABLED)
        BLOBM_Init();
#endif
        App_NvmInit();

        /* Init the app shell */
        app_shell_init();

        DBG_vPrintf(APP_DUAL_MODE_DEBUG, "Init BLE stack in progress ... \n");
        App_InitBle();

        DBG_vPrintf(APP_DUAL_MODE_DEBUG, "Init ZIGBEE stack in progress ... \n");
        App_InitZigbee();

        dm_switch_start();
    }
    /* Call application task */
    App_Thread( param );
}

/*! *********************************************************************************
* \brief  Runs the dual mode switch Idle Task
*
********************************************************************************** */
void dm_switch_IdleTask(void)
{
    dm_switch_RunZigbeeTasks();
}

/*! *********************************************************************************
* \brief  Returns the structure linked to a connectivity mode
*
* \param[in] mode: targeted mode
*
*
* \return    the targeted con mode structures.
*
********************************************************************************** */
const sConModeFunctions* dm_switch_get_con_mode_functions(eDualConMode mode)
{
    const sConModeFunctions* pResult = NULL;
    if (mode < (sizeof(bleZigbeeConSwitchFunctions)/sizeof(sConModeFunctions)))
    {
        pResult = &bleZigbeeConSwitchFunctions[mode];
    }
    return pResult;
}

/*! *********************************************************************************
* \brief  Returns the running connectivity mode
*
* \return    the running con mode.
*
********************************************************************************** */
eDualConMode dm_switch_get_running_con_mode()
{
    return (eDualConMode) app_running_con_mode;
}

/*! *********************************************************************************
* \brief  Returns the stopped connectivity mode
*
* \return    the not running connectivity mode
*
********************************************************************************** */
eDualConMode dm_switch_get_not_running_con_mode()
{
    return eZigbeeBLEMode;
}

/************************************************************************************
*************************************************************************************
* Private functions
*************************************************************************************
************************************************************************************/

#if !defined (SOTA_ENABLED)
#if !gUseHciTransportDownward_d
/* Called by BLE when a connect is received */
static void BLE_SignalFromISRCallback(void)
{
#if (cPWR_UsePowerDownMode) && (!cPWR_NoPowerDownDisabledOnConnect)
    PWR_DisallowDeviceToSleep();
#endif /* cPWR_UsePowerDownMode */
}
#endif /* !gUseHciTransportDownward_d */
#endif /* SOTA_ENABLED */

/*****************************************************************************
* Init the BLE stack and the BLE application
* Return value: None
*****************************************************************************/
static void App_InitBle()
{

#if !defined (SOTA_ENABLED)
#if !gUseHciTransportDownward_d
    pfBLE_SignalFromISR = BLE_SignalFromISRCallback;
#endif /* !gUseHciTransportDownward_d */
#endif /* SOTA_ENABLED */

    BleApp_Init();
    /* Prepare application input queue.*/
    MSG_InitQueue(&mHostAppInputQueue);
    MSG_InitQueue(&mAppCbInputQueue);

    /* BLE Host Stack Init */
    if (Ble_Initialize(App_GenericCallback) != gBleSuccess_c)
    {
        panic(0,0,0,0);
        return;
    }

}

/*****************************************************************************
* Init the zigbee stack and the zigbee application
* Return value: None
*****************************************************************************/
static void App_InitZigbee()
{
    /* Initialise LEDs and buttons */
    APP_vLedInitialise();
    APP_vInitResources();
    APP_vInitZigbeeResources();
    APP_vInitialiseCoordinator();
    BDB_vStart();
}

/*****************************************************************************
* Handles all key events for this device.
* Interface assumptions: None
* Return value: None
*****************************************************************************/
#if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0)
static void App_KeyboardCallBack
  (
  uint8_t events  /*IN: Events from keyboard module  */
  )
{
    switch (events)
    {
        case gKBD_EventPressPB2_c:
        {
            break;
        }
        default:
        {
            break;
        }
    }
}
#endif

static void dm_switch_RunZigbeeTasks()
{
    APP_vRunZigbee();
    ZTIMER_vTask();
    APP_taskCoordinator();
}


static void dm_switch_start(void)
{
    BleApp_Start();
}