/*! *********************************************************************************
* Copyright (c) 2014, Freescale Semiconductor, Inc.
* Copyright 2016-2020 NXP
*
* \file
*
* This is a source file for the main application Zigbee + BLE.
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

/************************************************************************************
*************************************************************************************
* Include
*************************************************************************************
************************************************************************************/

/* Framework include */
#include "fsl_os_abstraction.h"
#include "MemManager.h"
#include "TimersManager.h"
#include "RNG_Interface.h"
#include "Messaging.h"
#include "Flash_Adapter.h"
#include "SecLib.h"
#include "Panic.h"
#include "PDM.h"
#include "OtaSupport.h"
#include "PWR_Interface.h"
#include "PWR_Configuration.h"
#include "fsl_xcvr.h"

/* KSDK */
#include "board.h"
#include "fsl_ctimer.h"

/* zigbee includes */
#include "app_main.h"
#include "app_end_device_node.h"
#include "dbg.h"
#include "app_buttons.h"
#include "app_ota_client.h"
#include "zigbee_config.h"
#include "ZTimer.h"
#include "app_zcl_task.h"

/* ble includes */
#include "ble_init.h"
#include "controller_interface.h"

/* dual mode include */
#include "app_dual_mode_switch.h"

/************************************************************************************
*************************************************************************************
* Local macros
*************************************************************************************
************************************************************************************/

#define ZIGBEE_SWITCH_MARGIN_MS 10

/************************************************************************************
*************************************************************************************
* Private definitions
*************************************************************************************
************************************************************************************/
typedef enum {
    eZigbeeBLEMode = eConMode1,
} eDualModeZbBle;

/************************************************************************************
*************************************************************************************
* Private prototypes
*************************************************************************************
************************************************************************************/

static void App_InitBle(void);
static void App_InitZigbee(bool_t bColdStart);
static void dm_switch_runZigbeeTasks(void);
static void dm_switch_preSleepCallBack(void);
static void dm_switch_wakeupCallBack(void);

static void dm_switch_start(void);

/************************************************************************************
*************************************************************************************
* Private memory declarations
*************************************************************************************
************************************************************************************/
static const sConModeFunctions bleZigbeeConSwitchFunctions[]={
    /* ConMode0 is the zigbee/BLE mode  */
    {
        .dm_switch_mode = NULL,
        .dm_switch_start = dm_switch_start,
        .dm_switch_set_sleeping_status = NULL,
        .dm_reset_ota_state_machine = NULL,
    }
};

static uint8_t platformInitialized = 0;
static uint8_t app_running_con_mode = eZigbeeBLEMode;
static uint8_t zigbeeNbAdvbetweenSleep = 0;
static bool_t reinitZigbeeAfterSleep = FALSE;

/************************************************************************************
*************************************************************************************
* Exported functions/variables
*************************************************************************************
************************************************************************************/

/* BLE Application input queues */
extern anchor_t mHostAppInputQueue;
extern anchor_t mAppCbInputQueue;

/* BLE application extern function */
extern osaStatus_t AppIdle_TaskInit(void);
extern void App_Thread (uint32_t param);
extern void App_GenericCallback (gapGenericEvent_t* pGenericEvent);
extern void BleAppDrv_Init(bool reinit);
extern void BleApp_Start(void);

/* Framework extern function */
extern void StackTimer_Init(void (*cb)(void));
extern void StackTimer_ISR(void);

/* BLE app event */
osaEventId_t  mAppEvent;

extern struct fwk_cfg framework_configuration;

/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/

/*! *********************************************************************************
* \brief  This is the first task created by the OS. This task will initialize
*         the system
*
* \param[in]  param
*
********************************************************************************** */
void main_task(uint32_t param)
{
    if (!platformInitialized)
    {
        platformInitialized = 1;

        BOARD_SetFaultBehaviour();

        /* Framework init */
        MEM_Init();
        TMR_Init();

        /* Cryptographic and RNG hardware initialization */
        SecLib_Init();
        /* RNG software initialization and PRNG initial seeding (from hardware) */
        RNG_Init();
        RNG_SetPseudoRandomNoSeed(NULL);

        PWR_Init();
        PWR_ChangeDeepSleepMode(cPWR_PowerDown_RamRet);
        PWR_vForceRadioRetention(TRUE);
        /* Disable the device to sleep until adversting is started*/
        PWR_DisallowDeviceToSleep();
        /* Disable the device to sleep until the zigbee app is OK to sleep*/
        PWR_DisallowDeviceToSleep();

        PWR_RegisterLowPowerExitCallback(dm_switch_wakeupCallBack);
        PWR_RegisterLowPowerEnterCallback(dm_switch_preSleepCallBack);

        /* PDM init */
        PDM_Init();
        /* Init ADC */
        BOARD_InitAdc();

        /* Init the Idle task */
        AppIdle_TaskInit();

        /* Create application event */
        mAppEvent = OSA_EventCreate(TRUE);
        if( NULL == mAppEvent )
        {
            panic(0,0,0,0);
            return;
        }

        /* Init the OTA support module */
        OTA_ClientInit();

        SWITCH_DBG_LOG("Init BLE stack in progress ... \n");
        App_InitBle();

        SWITCH_DBG_LOG("Init ZIGBEE stack in progress ... \n");
        App_InitZigbee(TRUE);

        /* Set the wake up advance value to 10 ms */
        (*(uint8_t *) &framework_configuration.lp_cfg.wakeup_advance) = 17;
        dm_switch_start();
    }

    /* Call application task */
    App_Thread( param );
}

/*! *********************************************************************************
* \brief  Returns the structure linked to a connectivity mode
*
* \param[in] mode: targeted mode
*
*
* \return    the targeted con mode structures.
*
********************************************************************************** */
const sConModeFunctions* dm_switch_get_con_mode_functions(eDualConMode mode)
{
    const sConModeFunctions* pResult = NULL;
    if (mode < (sizeof(bleZigbeeConSwitchFunctions)/sizeof(sConModeFunctions)))
    {
        pResult = &bleZigbeeConSwitchFunctions[mode];
    }
    return pResult;
}

/*! *********************************************************************************
* \brief  Returns the running connectivity mode
*
* \return    the running con mode.
*
********************************************************************************** */
eDualConMode dm_switch_get_running_con_mode()
{
    return (eDualConMode) app_running_con_mode;
}

/*! *********************************************************************************
* \brief  Returns the stopped connectivity mode
*
* \return    the not running connectivity mode
*
********************************************************************************** */
eDualConMode dm_switch_get_not_running_con_mode()
{
    return (eDualConMode) eZigbeeBLEMode;
}

/*! *********************************************************************************
* \brief  Runs the dual mode switch Idle Task
*
********************************************************************************** */
void dm_switch_IdleTask(void)
{
    if (PWR_CheckIfDeviceCanGoToSleep())
    {
        zigbeeNbAdvbetweenSleep++;
        PWR_EnterLowPower();
    }
    else
    {
        if (reinitZigbeeAfterSleep)
        {
            vAppApiRestoreMacSettings();
            App_InitZigbee(FALSE);
            APP_vAlignStatesAfterSleep();
            reinitZigbeeAfterSleep = FALSE;
        }
        dm_switch_runZigbeeTasks();
    }
}

/************************************************************************************
*************************************************************************************
* Private functions
*************************************************************************************
************************************************************************************/

static void App_InitBle(void)
{

    bool_t bleStackInitStatus = FALSE;

    BleAppDrv_Init(false);
    /* Prepare application input queue.*/
    MSG_InitQueue(&mHostAppInputQueue);
    MSG_InitQueue(&mAppCbInputQueue);

    do
    {
        if (Ble_Initialize(App_GenericCallback) != gBleSuccess_c)
            break;
        bleStackInitStatus = TRUE;
    } while(0);

    if (!bleStackInitStatus)
    {
        panic(0,0,0,0);
    }
    BLE_disable_sleep();
}

static void App_InitZigbee(bool_t bColdStart)
{
    APP_vInitResources();
    APP_vInitZigbeeResources();
    /* Initialise application */
    APP_vInitialiseEndDevice(bColdStart);
    if(bColdStart)
    {
        BDB_vStart();
    }
    else
    {
        BDB_vRestart();
    }
}

static void dm_switch_runZigbeeTasks(void)
{
    APP_vRunZigbee();
    APP_taskEndDevicNode();
    ZTIMER_vTask();
}

static void dm_switch_preSleepCallBack(void)
{
    SWITCH_DBG_LOG("sleeping");
    if (zigbeeNbAdvbetweenSleep == 1)
    {
        /* If the power mode is with RAM held do the following
         * else not required as the entry point will init everything*/
         vSetOTAPersistedDatForMinRetention();
         /* sleep memory held */
         vAppApiSaveMacSettings();
    }
    /* Deinitialize the OTA support */
    OTA_DeInitExternalMemory();
    /* Deinitialize debug console */
    BOARD_DeinitDebugConsole();
    /* DeInitialize application support for drivers */
    BOARD_DeInitAdc();
    /* configure pins for power down mode */
    BOARD_SetPinsForPowerMode();
    /* DeInit the necessary clocks */
    BOARD_SetClockForPowerMode();
}

static void dm_switch_wakeupCallBack(void)
{
    RNG_Init();
    SecLib_Init();
    StackTimer_Init(StackTimer_ISR);
    OTA_InitExternalMemory();
    if (zigbeeNbAdvbetweenSleep >= 2)
    {
        reinitZigbeeAfterSleep = TRUE;
        zigbeeNbAdvbetweenSleep = 0;
        /* Disable the device to sleep until the zigbee app is OK to sleep*/
        PWR_DisallowDeviceToSleep();
    }
    BleAppDrv_Init(true);

    SWITCH_DBG_LOG("woken up \n");
}


static void dm_switch_start(void)
{
    BleApp_Start();
}