/*! *********************************************************************************
* Copyright (c) 2015, Freescale Semiconductor, Inc.
* Copyright 2016-2017 NXP
* All rights reserved.
*
* \file
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_i2c.h"
#include "fsl_ntag.h"
#include "Panic.h"
#include "TimersManager.h"
#include "MemManager.h"
#include "app_ntag.h"


/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define APP_I2C_NTAG_BASE                 I2C2
#define APP_I2C_NTAG_RESET                kFC6_RST_SHIFT_RSTn
#define I2C_MASTER_CLOCK_FREQUENCY        CLOCK_GetFreq(kCLOCK_Fro32M)


#define NTAG_I2C_BASE                     ((I2C_Type *) APP_I2C_NTAG_BASE)
#define NTAG_I2C_BAUDRATE                 400000U
#define NTAG_I2C_RESET                    APP_I2C_NTAG_RESET

/* NTAG power up time, when its VDD line is supplied */
#define NTAG_POWER_UP_DELAY_US            300

/* NTAG memory block size */
#define NTAG_BLOCK_SIZE                   16
#define NTAG_MAX_DEV_ADDR                 127

#define NDEF_DATA_BUFF_LEN                256
#define NO_ERROR                          0x00U
#define ERROR_STATE                       NO_ERROR + 1
#define AAR_FLAGS_INIT_FLAGS              0x00
#define NTAG_I2C_BLOCK_SIZE               0x10


#define NDEF_MSG_LEN_MAX_1K               880         // (page_55 - page_0) * 16_bytes/page
#define NDEF_MSG_LEN_MAX_2K               1904         // (page_127 - page_63) * 16_bytes/page + NDEF_MSG_LEN_MAX_1K
#define NDEF_MSG_LEN_1B_TLV               0x00FF
#define NDEF_MSG_LEN_3B_TLV               0xFFFF
#define NULL_TLV                          0x00
#define LOCK_CONTROL_TLV                  0x01
#define MEMORY_CONTROL_TLV                0x02
#define NDEF_MESSAGE_TLV                  0x03
#define PROPRIETARY_TLV                   0xFD
#define TERMINATOR_TLV                    0xFE
#define NDEF_MSG_TLV_LEN_LONG             4
#define NDEF_MSG_TLV_LEN_SHORT            2

#define APP_NTAG_DELAY_5MS                5    /* millisecond delay for NTAG writing */
#define APP_NTAG_BUFFER_NUM               4    /* buffer number for NTAG DATA */

//Index definition for pairing data array
#define    NTAG_TYPE_LENGTH_INDEX         1
#define NTAG_PAYLOAD_LENGTH_INDEX         2
#define NTAG_BD_ADDRESS_INDEX             37    // Index of bluetooth device address in NDEF data array
#define NTAG_LE_ROLE_VALUE_INDEX          46
#define NTAG_LOCAL_NAME_LEN_INDEX         47
#define NTAG_LOCAL_NAME_VALUE_INDEX       49


/*******************************************************************************
 * Variables
 ******************************************************************************/

typedef enum
{
    NDEF_RW_NO_ERROR = 0x00,
    NDEF_RW_ERR_NOT_T2T_FORMAT,
    NDEF_RW_ERR_TERM_TLV_MISSING,
    NDEF_RW_ERR_TOO_LONG_MSG,
    NDEF_RW_ERR_READ,
    NDEF_RW_ERR_WRITE
} NDEF_RW_STATUS_T;

/* state for data transmit */
enum
{
    STATE_IDLE,
    STATE_DATA_WRITING,
};


typedef struct
{
    uint8_t *pData;
    uint32_t dataWrAddr;
    uint32_t dataLen;
} ntagDataBuff_t;

typedef struct
{
    uint8_t slvAddr;                     /* NTAG device address */
    uint8_t *pData;                      /* Pointer points to data to be written */
    uint32_t dataWrAddr;                 /* data address in NTAG to be written */
    uint32_t blockId;                    /* block id to be read/written */
    uint32_t dataLen;                    /* data length */
    uint8_t frame[NTAG_BLOCK_SIZE+1];    /* data buffer for reading/writing */
    tmrTimerID_t delayTimer;             /* timer for NTAG operation delay */
    uint32_t procedureState;             /* state */
    uint32_t dataBuffId;                 /* Index of data buffer */
    uint32_t dataBuffCnt;                /* Counter of data buffer */
    ntagDataBuff_t dataBuff[APP_NTAG_BUFFER_NUM];
} ntagEnv_t;

ntagEnv_t mNtagEnv;

/*******************************************************************************
 * Code

 ******************************************************************************/

static void NtagApp_FieldDetectCb(ntag_field_detect_t fd, void *userData)
{
    PRINTF("FD callback: %d\n\r", fd == kNTAG_FieldDetectIn ? 1 : 0);
}

static void NtagApp_NtagEnable(bool on)
{
    i2c_master_config_t s_masterConfig;
    if (on)
    {
        /* Switch on NTAG, configure IO pads */
        NTAG_SetState(kNTAG_StateActive);

        /* Wait for NTAG to be ready */
        CLOCK_uDelay(NTAG_POWER_UP_DELAY_US);

        /* Reset NTAG I2C */
        RESET_PeripheralReset(NTAG_I2C_RESET);

        /*
         * masterConfig.debugEnable = false;
         * masterConfig.ignoreAck = false;
         * masterConfig.pinConfig = kI2C_2PinOpenDrain;
         * masterConfig.baudRate_Bps = 100000U;
         * masterConfig.busIdleTimeout_ns = 0;
         * masterConfig.pinLowTimeout_ns = 0;
         * masterConfig.sdaGlitchFilterWidth_ns = 0;
         * masterConfig.sclGlitchFilterWidth_ns = 0;
         */
        I2C_MasterGetDefaultConfig(&s_masterConfig);

        /* Change the default baudrate configuration */
        s_masterConfig.baudRate_Bps = NTAG_I2C_BAUDRATE;

        /* Initialize the I2C master peripheral */
        I2C_MasterInit(NTAG_I2C_BASE, &s_masterConfig, I2C_MASTER_CLOCK_FREQUENCY);
    }
    else
    {
        /* Disable I2C master */
        I2C_MasterDeinit(NTAG_I2C_BASE);

        /* Switch off NTAG, configure IO pads */
        NTAG_SetState(kNTAG_StateInactive);
    }
}


static int NtagApp_I2cWrite(uint32_t slvAddr, uint8_t *data, size_t len)
{
    i2c_master_transfer_t masterXfer;
    status_t status;
    int ret = 1;

    /* Initialize Transfer structure to default */
    memset(&masterXfer, 0, sizeof(masterXfer));

    /* Initialize Transfer structure with user data */
    masterXfer.slaveAddress = slvAddr;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = data;
    masterXfer.dataSize = len;
    masterXfer.flags = kI2C_TransferDefaultFlag;
    status = I2C_MasterTransferBlocking(NTAG_I2C_BASE, &masterXfer);

    if (status == kStatus_Success)
    {
        ret = 0;
    }

    return ret;
}


static int NtagApp_I2cRead(uint32_t slvAddr, uint8_t *data, size_t len)
{
    i2c_master_transfer_t masterXfer;
    status_t status;
    int ret = 1;

    /* Initialize Transfer structure to default */
    memset(&masterXfer, 0, sizeof(masterXfer));

    /* Initialize Transfer structure with user data */
    masterXfer.slaveAddress = slvAddr;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = data;
    masterXfer.dataSize = len;
    masterXfer.flags = kI2C_TransferDefaultFlag;
    status = I2C_MasterTransferBlocking(NTAG_I2C_BASE, &masterXfer);

    if (status == kStatus_Success)
    {
        ret = 0;
    }

    return ret;
}

#if 0
static int NtagApp_BlockWrite(uint32_t slvAddr, uint32_t blockid, const uint8_t *data)
{
    uint8_t frame[NTAG_BLOCK_SIZE + 1];
    int ret;

    frame[0] = (uint8_t) blockid;
    memcpy(&frame[1], data, NTAG_BLOCK_SIZE);

    ret = NtagApp_I2cWrite(slvAddr, frame, sizeof(frame));
    return ret;
}
#endif


static int NtagApp_BlockRead(uint32_t slvAddr, uint32_t blockid, uint8_t *data)
{
    uint8_t frame[1];
    int ret;

    frame[0] = (uint8_t) blockid;

    ret = NtagApp_I2cWrite(slvAddr, frame, sizeof(frame));

    if (ret == 0)
    {
        ret = NtagApp_I2cRead(slvAddr, data, NTAG_BLOCK_SIZE);
    }

    return ret;
}

static int NtagApp_DumpBlock0(uint32_t slvAddr)
{
    uint8_t buf[NTAG_BLOCK_SIZE];
    int ret = -1;

    if (NtagApp_BlockRead(slvAddr, 0, buf) == 0)
    {
        ret = 0;
    }

    return ret;
}


/*! *********************************************************************************
* \brief        Write data to NTAG,
*
* \param[in]    parameters pointer
********************************************************************************** */
void NtagApp_BytesWrite(void *param)
{
    if(mNtagEnv.procedureState == STATE_IDLE)
    {
        if(mNtagEnv.dataBuffId >= mNtagEnv.dataBuffCnt)
        {
            NtagApp_NtagEnable(FALSE);    /* it shall be disabled after all data had been written completely */
            return;    //write complete
        }
        mNtagEnv.pData = mNtagEnv.dataBuff[mNtagEnv.dataBuffId].pData;
        mNtagEnv.dataWrAddr = mNtagEnv.dataBuff[mNtagEnv.dataBuffId].dataWrAddr;
        mNtagEnv.dataLen = mNtagEnv.dataBuff[mNtagEnv.dataBuffId].dataLen;
        mNtagEnv.procedureState = STATE_DATA_WRITING;
    }

    if(mNtagEnv.procedureState == STATE_DATA_WRITING)
    {
        uint32_t blockOffset = mNtagEnv.dataWrAddr % NTAG_BLOCK_SIZE;
        mNtagEnv.blockId = mNtagEnv.dataWrAddr / NTAG_BLOCK_SIZE;
        if(blockOffset != 0 || mNtagEnv.dataLen < (NTAG_BLOCK_SIZE-blockOffset))
        {
            if(NtagApp_BlockRead(mNtagEnv.slvAddr, mNtagEnv.blockId, &mNtagEnv.frame[1]))    /* read the target block content */
            {
                panic(0,0,0,0);    /* read failed */
            }
        }
        mNtagEnv.frame[0] = mNtagEnv.blockId;
        uint32_t copyLen = (NTAG_BLOCK_SIZE-blockOffset)<mNtagEnv.dataLen?(NTAG_BLOCK_SIZE-blockOffset):mNtagEnv.dataLen;
        memcpy(&mNtagEnv.frame[1+blockOffset], mNtagEnv.pData, copyLen);
        if(NtagApp_I2cWrite(mNtagEnv.slvAddr, mNtagEnv.frame, sizeof(mNtagEnv.frame)))
        {
            PRINTF("WRITE failed\r\n");
            panic(0,0,0,0);    /* write failed */
        }
        else
        {
            mNtagEnv.pData += copyLen;
            mNtagEnv.dataLen -= copyLen;
            mNtagEnv.dataWrAddr += copyLen;
            if(mNtagEnv.dataLen == 0)
            {
                mNtagEnv.procedureState = STATE_IDLE;
                MEM_BufferFree(mNtagEnv.dataBuff[mNtagEnv.dataBuffId].pData);    //Free memory
                mNtagEnv.dataBuff[mNtagEnv.dataBuffId].pData = NULL;
                mNtagEnv.dataBuffId++;
            }

            /* Start a 5ms delay timer */
            TMR_StartTimer(mNtagEnv.delayTimer, gTmrSingleShotTimer_c, APP_NTAG_DELAY_5MS, NtagApp_BytesWrite, NULL);
        }
    }

}

/*! *********************************************************************************
* \brief        Start to write data to NTAG
********************************************************************************** */
void NtagApp_StartToWrite(void)
{
    mNtagEnv.dataBuffId = 0;
    NtagApp_NtagEnable(TRUE);
    NtagApp_BytesWrite(NULL);
}

/*! *********************************************************************************
* \brief        Update NDEF pairing data and write data to NTAG
*
* \param[out]      Write successful or failed
********************************************************************************** */
NDEF_RW_STATUS_T NtagApp_MsgConfig(enum leRole_tag role, char *pLocalName, uint32_t localNameLen)
{
    bool boRemainData = TRUE;
    uint16_t wOutBuffLenTmp = 0;
    uint8_t bTemp = 0;

    mNtagEnv.dataBuffCnt = 0;    //Reset the data buffer counter

    uint8_t blePairingNdefData[] =
    {
        0xD2,    // NDEF Record Header
        0x20,    // Record Type length
        0x0E,    // payload length without local name value
        // Record Type Name: "application/vnd.bluetooth.le.oob"
        0x61, 0x70, 0x70, 0x6C, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x76, 0x6E,
        0x64, 0x2E, 0x62, 0x6C, 0x75, 0x65, 0x74, 0x6F, 0x6F, 0x74, 0x68, 0x2E, 0x6C, 0x65,
        0x2E, 0x6F, 0x6F, 0x62,
        0x08,    // LE Bluetooth Device Address length: 8 bytes
        0x1B,    // LE Bluetooth Device Address data type
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Bluetooth Device Address: 6 bytes
        0x00,    // added at the end of the Device Address
        0x02,    // LE Role Length: 2 bytes
        0x1C,    // LE Role data type
        0x02,    // LE Role: PeripheralPrefered
        0x08,    // LE Local Name length
        0x09,    // LE Local Name data type
    };

    uint8_t defaultEmptyNdef[] =
    {
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0xE1, 0x10, 0x6D, 0x00,
        0x03, 0x03, 0xD0, 0x00,
        0x00, 0xFE, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
    };

    uint8_t endNdefTag = TERMINATOR_TLV;    //end tag of NDEF

    //default empty NDEF data
    uint32_t defaultEmptyNdefLen = sizeof(defaultEmptyNdef);
    uint8_t *pDefaultEmptyNdef = MEM_BufferAlloc(defaultEmptyNdefLen);
    if(pDefaultEmptyNdef == NULL)
    {
        panic(0,0,0,0);        //allocate failed
    }
    else
    {
        memcpy(pDefaultEmptyNdef, defaultEmptyNdef, defaultEmptyNdefLen);
    }

    //pairing data
    uint32_t blePairingNdefDataLen = sizeof(blePairingNdefData)+localNameLen;
    uint8_t *pBlePairingNdefDataBuffer = MEM_BufferAlloc(blePairingNdefDataLen);    //allocate memory for blePairingNdefData
    if(pBlePairingNdefDataBuffer == NULL)
    {
        panic(0,0,0,0);        //allocate failed
    }
    else
    {
        /* copy the device address to the NDEF message field */
        uint8_t bdAddr[6], bdAddrLen = 0;
        BOARD_GetMCUUid(bdAddr, &bdAddrLen);

        blePairingNdefData[NTAG_PAYLOAD_LENGTH_INDEX] = blePairingNdefData[NTAG_PAYLOAD_LENGTH_INDEX] + localNameLen;    //update payload length with local name length
        memcpy(&blePairingNdefData[NTAG_BD_ADDRESS_INDEX], bdAddr, BD_ADDR_SIZE);        //Update bd address
        blePairingNdefData[NTAG_LE_ROLE_VALUE_INDEX] = (uint8_t)role;                    //update role
        blePairingNdefData[NTAG_LOCAL_NAME_LEN_INDEX] = localNameLen+1;                    //update local name length + flag length (1 byte)
        memcpy(pBlePairingNdefDataBuffer, blePairingNdefData, sizeof(blePairingNdefData));
        memcpy(&pBlePairingNdefDataBuffer[NTAG_LOCAL_NAME_VALUE_INDEX], (uint8_t *)pLocalName, localNameLen);    //update local name to data buffer
    }

    //end tag of NDEF
    uint32_t endNdefTagLen = sizeof(endNdefTag);
    uint8_t *pEndNdefTag = MEM_BufferAlloc(endNdefTagLen);
    if(pEndNdefTag == NULL)
    {
        panic(0,0,0,0);        //allocate failed
    }
    else
    {
        memcpy(pEndNdefTag, &endNdefTag, endNdefTagLen);
    }

    //Block one data
    uint8_t *pBlockOneDataBuffer = MEM_BufferAlloc(NTAG_BLOCK_SIZE);
    if(pBlockOneDataBuffer == NULL)
    {
        panic(0,0,0,0);        //allocate failed
    }


    /* check the length of the NDEF message, which will be written */
    if((blePairingNdefDataLen + 3) > NDEF_MSG_LEN_MAX_2K)
    {
        return NDEF_RW_ERR_TOO_LONG_MSG;
    }
    /* set the zero data to the buffer for block 1 */
    memset(pBlockOneDataBuffer, 0, NTAG_I2C_BLOCK_SIZE);
    /* fill the NDEF message control char */
    pBlockOneDataBuffer[0] = NDEF_MESSAGE_TLV;
    /* calculate the TLV length */
    if((blePairingNdefDataLen +3) >= NDEF_MSG_LEN_1B_TLV)
    {
        /* retype the length of the message*/
        wOutBuffLenTmp = (uint16_t)blePairingNdefDataLen;
        /* fill the TLV message length */
        pBlockOneDataBuffer[1] = (uint8_t)NDEF_MSG_LEN_1B_TLV;
        pBlockOneDataBuffer[2] = (uint8_t)((wOutBuffLenTmp & 0xFF00) >> 8);
        pBlockOneDataBuffer[3] = (uint8_t)(wOutBuffLenTmp & 0x00FF);
        /* copy the remain data to the buffer for block 1 */
        memcpy(&pBlockOneDataBuffer[4], pBlePairingNdefDataBuffer, (NTAG_I2C_BLOCK_SIZE-4) );
        /* recalculate the remain length of the NDEF message */
        wOutBuffLenTmp -= (NTAG_I2C_BLOCK_SIZE-4);
        pBlePairingNdefDataBuffer += (NTAG_I2C_BLOCK_SIZE-4);
    }
    else
    {
        /* fill the TLV message length */
        pBlockOneDataBuffer[1] = (uint8_t)blePairingNdefDataLen;
        /* copy the remain data to the buffer for block 1 */
        if(pBlockOneDataBuffer[1]>(NTAG_I2C_BLOCK_SIZE-2))
        {
            memcpy(&pBlockOneDataBuffer[2], pBlePairingNdefDataBuffer, (NTAG_I2C_BLOCK_SIZE-2) );
            /* recalculate the remain length of the NDEF message */
            wOutBuffLenTmp = pBlockOneDataBuffer[1] - (NTAG_I2C_BLOCK_SIZE-2);
            pBlePairingNdefDataBuffer += (NTAG_I2C_BLOCK_SIZE-2); // 17f74
        }
        else
        {
            memcpy(&pBlockOneDataBuffer[2], pBlePairingNdefDataBuffer, pBlockOneDataBuffer[1] );
            pBlockOneDataBuffer[pBlockOneDataBuffer[1]+2] = TERMINATOR_TLV;
            boRemainData = FALSE;
            /* remain length of the NDEF message is 0 */
            wOutBuffLenTmp = 0;
            /* whole NDEF message + LTV bytes fits into the first user memory block */
            mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].pData = pBlockOneDataBuffer;
            mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataLen = NTAG_BLOCK_SIZE;
            mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataWrAddr = 16;    /* write into block 1 */
            mNtagEnv.dataBuffCnt++;
        }
    }
    if (boRemainData)
    {
        bTemp = wOutBuffLenTmp / NTAG_I2C_BLOCK_SIZE;
        /* check if longer message then available memory size */
        if (bTemp < 56)
        {
            /* check if length is equal the full user memory range */
            if (bTemp == 55)
            {
                if ((wOutBuffLenTmp % NTAG_I2C_BLOCK_SIZE))
                {
                    /* length is 55,xx blocks */
                    boRemainData = FALSE;
                }
            }
        }
        else
        {
            /* message is longer than 55x16 bytes (> 1k version) */
            boRemainData = FALSE;
        }
    }
    if (boRemainData)
    {
        uint32_t dataAddr = 0;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].pData = pDefaultEmptyNdef;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataLen = defaultEmptyNdefLen;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataWrAddr = dataAddr;
        mNtagEnv.dataBuffCnt++;
        dataAddr += defaultEmptyNdefLen;

        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].pData = pBlePairingNdefDataBuffer;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataLen = wOutBuffLenTmp;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataWrAddr = dataAddr;
        mNtagEnv.dataBuffCnt++;
        dataAddr += wOutBuffLenTmp;

        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].pData = pEndNdefTag;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataLen = endNdefTagLen;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataWrAddr = dataAddr;
        mNtagEnv.dataBuffCnt++;
        dataAddr += endNdefTagLen;

        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].pData = pBlockOneDataBuffer;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataLen = NTAG_BLOCK_SIZE;
        mNtagEnv.dataBuff[mNtagEnv.dataBuffCnt].dataWrAddr = 16;    /* write into block 1 */
        mNtagEnv.dataBuffCnt++;
    }

    return NDEF_RW_NO_ERROR;

}

/*! *********************************************************************************
* \brief        Write message into NTAG
*
* \param[in]    LE role
* \param[in]    pLocalName Pointer points to local name
* \param[in]    localNameLen Length of local name
********************************************************************************** */
void NtagApp_NdefPairingWr(enum leRole_tag role, char *pLocalName, uint32_t localNameLen)
{
    NtagApp_MsgConfig(role, pLocalName, localNameLen);

    NtagApp_StartToWrite();
}

/*! *********************************************************************************
* \brief        Get NTAG device address by polling reading
*
* \param[out]      NTAG device address
********************************************************************************** */
uint8_t NtagApp_GetAddr(void)
{
    uint8_t address = 0;
    for (address = 0; address <= NTAG_MAX_DEV_ADDR; address++)
    {
        if (NtagApp_DumpBlock0(address) == 0)
        {
            PRINTF("Found NTAG I2C address: %d\r\n", address);
            break;
        }
    }

    if (address > NTAG_MAX_DEV_ADDR)
    {
        PRINTF("NTAG address get failed\r\n");
        panic(0, 0, 0, 0);    /* No valid NTAG found */
    }

    return address;
}

void NtagApp_Init(void)
{
    ntag_config_t ntagConfig;
    NTAG_GetDefaultConfig(&ntagConfig);
    ntagConfig.userData = NULL;
    ntagConfig.callback = NtagApp_FieldDetectCb;

    /* Init NTAG driver: pads, FD handling */
    NTAG_Init(&ntagConfig);

    mNtagEnv.delayTimer = TMR_AllocateTimer();
    if(mNtagEnv.delayTimer == gTmrInvalidTimerID_c)
    {
        PRINTF("allocate timer failed\r\n");
        panic(0,0,0,0);
    }

    NtagApp_NtagEnable(true);
    mNtagEnv.slvAddr = NtagApp_GetAddr();
    NtagApp_NtagEnable(false);
}
