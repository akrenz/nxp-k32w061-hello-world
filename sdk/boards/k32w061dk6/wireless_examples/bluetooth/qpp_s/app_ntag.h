/*! *********************************************************************************
* Copyright (c) 2015, Freescale Semiconductor, Inc.
* Copyright 2016-2017 NXP
* All rights reserved.
*
* \file
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

#ifndef _APP_NTAG_H_
#define _APP_NTAG_H_

///The LE Role data value
enum leRole_tag
{
    PERIPHERAL_ROLE_ONLY = 0x00,
    CENTRAL_ROLE_ONLY,
    PERIPHERAL_AND_CENTRAL_ROLE,
    PERIPHERAL_AND_CENTRAL_FOR_CONN,
    RESERVED_ROLE,
};

#define NTAG_LOCAL_DEV_NAME     "FSL_QPP"


void NtagApp_Init(void);
void NtagApp_NdefPairingWr(enum leRole_tag role, char *pLocalName, uint32_t localNameLen);

#endif	/* #ifndef _APP_NTAG_H_ */
