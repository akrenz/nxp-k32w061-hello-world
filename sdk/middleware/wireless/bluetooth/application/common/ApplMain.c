/*! *********************************************************************************
* Copyright (c) 2014, Freescale Semiconductor, Inc.
* Copyright 2016-2019 NXP
*
* \file
*
* This is a source file for the main application.
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

/************************************************************************************
*************************************************************************************
* Include
*************************************************************************************
************************************************************************************/
/* Drv */
#include "LED.h"

/* Fwk */
#include "fsl_os_abstraction.h"
#include "MemManager.h"
#include "TimersManager.h"
#include "RNG_Interface.h"
#include "Messaging.h"
#include "Flash_Adapter.h"
#include "SecLib.h"
#include "Panic.h"

#if defined(gFsciIncluded_c) && (gFsciIncluded_c == 1)
#include "FsciInterface.h"
#if gFSCI_IncludeLpmCommands_c
#include "FsciCommands.h"
#endif
#endif

/* KSDK */
#include "board.h"
#if defined(MULTICORE_CONNECTIVITY_CORE) && (MULTICORE_CONNECTIVITY_CORE)
  #if FSL_FEATURE_SOC_CAU3_COUNT
  #include "fsl_cau3.h"
  #endif
#endif
#include "fsl_rtc.h"
/* Bluetooth Low Energy */
#include "gatt_interface.h"
#include "gatt_server_interface.h"
#include "gatt_client_interface.h"
#include "gap_interface.h"
#include "ble_init.h"
#include "ble_config.h"
#include "l2ca_cb_interface.h"
#include "ble_constants.h"

#if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode)
#include "PWR_Interface.h"
#endif
#if defined(cPWR_FullPowerDownMode) && (cPWR_FullPowerDownMode) && (configUSE_TICKLESS_IDLE != 0)
#include "TMR_Adapter.h"
#endif

#if gUsePdm_d
#include "PDM.h"
#endif
#ifdef FSL_RTOS_FREE_RTOS
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "portmacro.h"
#endif

#include "ApplMain.h"

#if (defined(CPU_QN908X) || defined(CPU_JN518X))
#include "controller_interface.h"
#endif
#ifdef CPU_QN908X
#include "fsl_wdt.h"
#include "clock_config.h"
#if (defined(BOARD_XTAL1_CLK_HZ) && (BOARD_XTAL1_CLK_HZ != CLK_XTAL_32KHZ))
#include "rco32k_calibration.h"
#endif /* BOARD_XTAL1_CLK_HZ != CLK_XTAL_32KHZ */
#endif /* CPU_QN908X */

#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) || (gFsciBleTest_d == 1))
#include "fsci_ble_gap.h"
#include "fsci_ble_gatt.h"
#include "fsci_ble_l2cap_cb.h"
#endif

#if (!cPWR_UsePowerDownMode)
#include "fsl_xcvr.h"
#endif

#if gOTA_InitAtBoot_d
#include "OtaSupport.h"
#endif

#if defined (SOTA_ENABLED)
#include "blob_manager_app.h"
#endif

#ifdef DUAL_MODE_APP
#include "app_dual_mode_switch.h"
#endif

/************************************************************************************
*************************************************************************************
* Private macros
*************************************************************************************
************************************************************************************/

/* Application Events */
#define gAppEvtMsgFromHostStack_c       (1U << 0U)
#define gAppEvtAppCallback_c            (1U << 1U)

#ifdef FSL_RTOS_FREE_RTOS
    #if (configUSE_IDLE_HOOK)
        #define mAppIdleHook_c 1
    #endif

    #if (configUSE_TICKLESS_IDLE != 0)
        #define mAppEnterLpFromIdleTask_c (0)
        #define mAppTaskWaitTime_c        (1000U) /* milliseconds */
        #define mAppOverheadTicks_c       (1U)    /* Application processing overhead in OS ticks */
    #endif
#endif

#ifndef mAppIdleHook_c
#define mAppIdleHook_c 0
#endif

#ifndef mAppEnterLpFromIdleTask_c
#define mAppEnterLpFromIdleTask_c 1
#endif

#ifndef mAppTaskWaitTime_c
#define mAppTaskWaitTime_c (osaWaitForever_c)
#endif

#if defined (MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE)
#define MULTICORE_STATIC
#else
#if defined DUAL_MODE_APP
#define MULTICORE_STATIC
#else
#define MULTICORE_STATIC static
#endif
#endif


/************************************************************************************
*************************************************************************************
* Private type definitions
*************************************************************************************
************************************************************************************/
/* Host to Application Messages Types */
typedef enum {
    gAppGapGenericMsg_c = 0,
    gAppGapConnectionMsg_c,
    gAppGapAdvertisementMsg_c,
    gAppGapScanMsg_c,
    gAppGattServerMsg_c,
    gAppGattClientProcedureMsg_c,
    gAppGattClientNotificationMsg_c,
    gAppGattClientIndicationMsg_c,
    gAppL2caLeDataMsg_c,
    gAppL2caLeControlMsg_c,
    gAppSecLibMultiplyMsg_c,
}appHostMsgType_t;

/* Host to Application Connection Message */
typedef struct connectionMsg_tag{
    deviceId_t              deviceId;
    gapConnectionEvent_t    connEvent;
}connectionMsg_t;

/* Host to Application GATT Server Message */
typedef struct gattServerMsg_tag{
    deviceId_t          deviceId;
    gattServerEvent_t   serverEvent;
}gattServerMsg_t;

/* Host to Application GATT Client Procedure Message */
typedef struct gattClientProcMsg_tag{
    deviceId_t              deviceId;
    gattProcedureType_t     procedureType;
    gattProcedureResult_t   procedureResult;
    bleResult_t             error;
}gattClientProcMsg_t;

/* Host to Application GATT Client Notification/Indication Message */
typedef struct gattClientNotifIndMsg_tag{
    uint8_t*    aValue;
    uint16_t    characteristicValueHandle;
    uint16_t    valueLength;
    deviceId_t  deviceId;
}gattClientNotifIndMsg_t;

/* L2ca to Application Data Message */
typedef struct l2caLeCbDataMsg_tag{
    deviceId_t  deviceId;
    uint16_t    channelId;
    uint16_t    packetLength;
    uint8_t     aPacket[1];
}l2caLeCbDataMsg_t;

/* Sec Lib to Application Data Message */
typedef struct secLibMsgData_tag{
    computeDhKeyParam_t *pData;
} secLibMsgData_t;

typedef struct appMsgFromHost_tag{
    appHostMsgType_t    msgType;
    union {
        gapGenericEvent_t       genericMsg;
        gapAdvertisingEvent_t   advMsg;
        connectionMsg_t         connMsg;
        gapScanningEvent_t      scanMsg;
        gattServerMsg_t         gattServerMsg;
        gattClientProcMsg_t     gattClientProcMsg;
        gattClientNotifIndMsg_t gattClientNotifIndMsg;
        l2caLeCbDataMsg_t       l2caLeCbDataMsg;
        l2capControlMessage_t   l2caLeCbControlMsg;
        secLibMsgData_t         secLibMsgData;
    } msgData;
}appMsgFromHost_t;

typedef struct appMsgCallback_tag{
    appCallbackHandler_t   handler;
    appCallbackParam_t     param;
}appMsgCallback_t;
/************************************************************************************
*************************************************************************************
* Private prototypes
*************************************************************************************
************************************************************************************/
/* Actions required to be performed in IdleTask*/
#if defined DUAL_MODE_APP
#define IdleTask_DualMode_c   1
#else
#define IdleTask_DualMode_c   0
#endif
#if (defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode) && mAppEnterLpFromIdleTask_c)
#define IdleTask_LowPower_c 2
#else
#define IdleTask_LowPower_c 0
#endif

/* Board specific treatment is mandated if Xtal 32M temperature compensation is required.
 * Otherwise a default WEAK implementation is defined in this file.
 */
#define  IdleTask_BoardSpecific_c 8

#define IdleTaskAct_c (IdleTask_DualMode_c | IdleTask_LowPower_c | IdleTask_BoardSpecific_c)

#if (IdleTaskAct_c != 0)
  #if (mAppIdleHook_c)
    #define AppIdle_TaskInit()
    #define App_Idle_Task()
  #else /* mAppIdleHook_c */
    #if (IdleTask_DualMode_c)
    osaStatus_t AppIdle_TaskInit(void);
    #else
    static osaStatus_t AppIdle_TaskInit(void);
    #endif
    static void App_Idle_Task(osaTaskParam_t argument);
  #endif /* mAppIdleHook_c */
#endif

#if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0) && !defined (DUAL_MODE_APP)
static void App_KeyboardCallBack(uint8_t events);
#endif

static void App_HandleHostMessageInput(appMsgFromHost_t* pMsg);


MULTICORE_STATIC void App_ConnectionCallback (deviceId_t peerDeviceId, gapConnectionEvent_t* pConnectionEvent);
MULTICORE_STATIC void App_AdvertisingCallback (gapAdvertisingEvent_t* pAdvertisingEvent);
MULTICORE_STATIC void App_ScanningCallback (gapScanningEvent_t* pScanningEvent);
MULTICORE_STATIC void App_GattServerCallback (deviceId_t peerDeviceId, gattServerEvent_t* pServerEvent);
MULTICORE_STATIC void App_GattClientProcedureCallback
(
    deviceId_t              deviceId,
    gattProcedureType_t     procedureType,
    gattProcedureResult_t   procedureResult,
    bleResult_t             error
);
MULTICORE_STATIC void App_GattClientNotificationCallback
(
    deviceId_t      deviceId,
    uint16_t        characteristicValueHandle,
    uint8_t*        aValue,
    uint16_t        valueLength
);
MULTICORE_STATIC void App_GattClientIndicationCallback
(
    deviceId_t      deviceId,
    uint16_t        characteristicValueHandle,
    uint8_t*        aValue,
    uint16_t        valueLength
);

MULTICORE_STATIC void App_L2caLeDataCallback
(
    deviceId_t deviceId,
    uint16_t channelId,
    uint8_t* pPacket,
    uint16_t packetLength
);

MULTICORE_STATIC void App_L2caLeControlCallback
(
    l2capControlMessage_t* pMessage
);

MULTICORE_STATIC void App_SecLibMultCallback
(
    computeDhKeyParam_t *pData
);

#if !defined (SOTA_ENABLED) && !defined (DUAL_MODE_APP)
#if !defined(gUseHciTransportDownward_d) || (!gUseHciTransportDownward_d)
static void BLE_SignalFromISRCallback(void);
extern void (*pfBLE_SignalFromISR)(void);
#endif
#endif /* SOTA_ENABLED */

#if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode) && (configUSE_TICKLESS_IDLE != 0)
extern void vTaskStepTick( const TickType_t xTicksToJump );
#endif


/*! *********************************************************************************
*************************************************************************************
* Public prototypes
*************************************************************************************
********************************************************************************** */
void App_Thread (uint32_t param);


#if defined(gMWS_Enabled_d) && (gMWS_Enabled_d)
extern void App_Init(void);
#endif

#if !defined(MULTICORE_CONNECTIVITY_CORE) || (!MULTICORE_CONNECTIVITY_CORE)
void App_GenericCallback (gapGenericEvent_t* pGenericEvent);
#endif

/************************************************************************************
*************************************************************************************
* Private memory declarations
*************************************************************************************
************************************************************************************/
#if (IdleTaskAct_c != 0)
#if (!mAppIdleHook_c)
static OSA_TASK_DEFINE( App_Idle_Task, gAppIdleTaskPriority_c, 1, gAppIdleTaskStackSize_c, FALSE );
static osaTaskId_t mAppIdleTaskId = NULL;
#endif
#endif  /* cPWR_UsePowerDownMode */



typedef struct {
    uint16_t pdmId;
    uint16_t nbRamWrite;
    bleBondIdentityHeaderBlob_t  aBondingHeader;
    bleBondDataDynamicBlob_t     aBondingDataDynamic;
    bleBondDataStaticBlob_t      aBondingDataStatic;
    bleBondDataDeviceInfoBlob_t  aBondingDataDeviceInfo;
    bleBondDataDescriptorBlob_t  aBondingDataDescriptor[gcGapMaximumSavedCccds_c];
} bleBondDeviceEntry;

#if (gMaxBondedDevices_c > 0)
static bleBondDeviceEntry bondEntries[gMaxBondedDevices_c];
static osaMutexId_t bondingMutex;
#endif

#ifndef DUAL_MODE_APP
static osaEventId_t  mAppEvent;
/* Application input queues */
static anchor_t mHostAppInputQueue;
static anchor_t mAppCbInputQueue;
#else
extern osaEventId_t  mAppEvent;
anchor_t mHostAppInputQueue;
anchor_t mAppCbInputQueue;
#endif

static gapGenericCallback_t pfGenericCallback = NULL;
static gapAdvertisingCallback_t pfAdvCallback = NULL;
static gapScanningCallback_t pfScanCallback = NULL;
static gapConnectionCallback_t  pfConnCallback = NULL;
static gattServerCallback_t pfGattServerCallback = NULL;
static gattClientProcedureCallback_t pfGattClientProcCallback = NULL;
static gattClientNotificationCallback_t pfGattClientNotifCallback = NULL;
static gattClientNotificationCallback_t pfGattClientIndCallback = NULL;
static l2caLeCbDataCallback_t           pfL2caLeCbDataCallback = NULL;
static l2caLeCbControlCallback_t        pfL2caLeCbControlCallback = NULL;

/************************************************************************************
*************************************************************************************
* Public memory declarations
*************************************************************************************
************************************************************************************/

extern const uint8_t gUseRtos_c;

#ifdef CPU_QN908X
#if (defined(BOARD_XTAL1_CLK_HZ) && (BOARD_XTAL1_CLK_HZ != CLK_XTAL_32KHZ))
extern volatile bool_t gRco32kCalibrationRequest;
extern void RCO32K_Calibrate(void);
#endif /* BOARD_XTAL1_CLK_HZ  */
#endif /* CPU_QN908X */
/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/

#if !defined(gHybridApp_d) || (!gHybridApp_d)
#ifndef DUAL_MODE_APP
/*! *********************************************************************************
* \brief  This is the first task created by the OS. This task will initialize
*         the system
*
* \param[in]  param
*
********************************************************************************** */
void main_task(uint32_t param)
{
    static bool_t platformInitialized = FALSE;

    if (FALSE == platformInitialized)
    {
        platformInitialized = TRUE;

#if !defined(CPU_QN908X) && !defined (CPU_JN518X) && defined(gDCDC_Enabled_d) && (gDCDC_Enabled_d)
        /* Init DCDC module */
        BOARD_DCDCInit();
#endif
#if defined (CPU_JN518X)
        BOARD_SetFaultBehaviour();
#endif

        (void)MEM_Init();

        /* Framework init */
#if defined(gRngSeedStorageAddr_d) || defined(gXcvrDacTrimValueSorageAddr_d)\
     || gAppUseNvm_d ||  (defined gRadioUsePdm_d && gRadioUsePdm_d)
        NV_Init();
#endif

        TMR_Init();
        /* Check if RTC was started already: if not do it now */
        if (RTC->CTRL & RTC_CTRL_SWRESET_MASK)
        {
            CLOCK_EnableClock(kCLOCK_Rtc);
            CLOCK_SetClkDiv(kCLOCK_DivRtcClk, 32, false);    /* 1ms / 1kHz tick */
            CLOCK_SetClkDiv(kCLOCK_DivRtc1HzClk, 1U, false);
            RTC_Init(RTC);
            PWR_Start32kCounter();
        }

        /* Cryptographic and RNG hardware initialization */
  #if defined(MULTICORE_CONNECTIVITY_CORE) && (MULTICORE_CONNECTIVITY_CORE)
    #if defined (FSL_FEATURE_SOC_MMCAU_COUNT) && (FSL_FEATURE_SOC_MMCAU_COUNT > 0)
        /* Make sure the clock is provided by M0+ in order to avoid issues after exiting low power mode from M0+ */
        CLOCK_EnableClock(kCLOCK_Cau3);
      #if defined(FSL_CAU3_USE_HW_SEMA) && (FSL_CAU3_USE_HW_SEMA > 0)
        CLOCK_EnableClock(FSL_CAU3_SEMA42_CLOCK_NAME);
      #endif /* FSL_CAU3_USE_HW_SEMA */
    #endif /* FSL_FEATURE_SOC_MMCAU_COUNT */
  #else
        SecLib_Init();
  #endif /* MULTICORE_CONNECTIVITY_CORE */
        /* Set external multiplication callback if we don't have support for hardware elliptic curve
         * multiplication */
#if defined(FSL_FEATURE_SOC_CAU3_COUNT) && (FSL_FEATURE_SOC_CAU3_COUNT > 0)
#else
        SecLib_SetExternalMultiplicationCb(App_SecLibMultCallback);
#endif
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE)
        Board_StartSecondaryCoreApp();
        CLOCK_DisableClock(kCLOCK_Cau3);
#else
        /* RNG software initialization and PRNG initial seeding (from hardware) */
        (void)RNG_Init();
        RNG_SetPseudoRandomNoSeed(NULL);
#endif /* MULTICORE_APPLICATION_CORE */
        LED_Init();
  #if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0)
        KBD_Init(App_KeyboardCallBack);
  #endif

  #if (defined gRadioUsePdm_d && gRadioUsePdm_d) || (defined gBleControllerUsePdm_d && gBleControllerUsePdm_d)
        PDM_Init();
  #endif

#if !defined (SOTA_ENABLED)
#if !gUseHciTransportDownward_d
        pfBLE_SignalFromISR = BLE_SignalFromISRCallback;
#endif /* !gUseHciTransportDownward_d */
#endif /* SOTA_ENABLED */

#if (IdleTaskAct_c != 0) /* Idle task required */
#if (!mAppIdleHook_c)
        (void)AppIdle_TaskInit();
#endif
#endif
  #if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode)
        PWR_Init();
        PWR_DisallowDeviceToSleep();
  #else
        Led1Flashing();
        Led2Flashing();
        Led3Flashing();
        Led4Flashing();
  #endif

  #if defined (SOTA_ENABLED)
        BLOBM_Init();
  #endif
        /* Initialize peripheral drivers specific to the application */
        BleApp_Init();

        /* Create application event */
        mAppEvent = OSA_EventCreate(TRUE);
        if( NULL == mAppEvent )
        {
            panic(0,0,0,0);
            return;
        }

        /* Prepare application input queue.*/
        MSG_InitQueue(&mHostAppInputQueue);

        /* Prepare callback input queue.*/
        MSG_InitQueue(&mAppCbInputQueue);

        App_NvmInit();
  #if !defined(MULTICORE_CONNECTIVITY_CORE) || (!MULTICORE_CONNECTIVITY_CORE)
        /* BLE Host Stack Init */
        if (Ble_Initialize(App_GenericCallback) != gBleSuccess_c)
        {
            panic(0,0,0,0);
            return;
        }
  #endif /* MULTICORE_CONNECTIVITY_CORE */

  #if defined(gMWS_Enabled_d) && (gMWS_Enabled_d)
        App_Init();
  #endif
    }

    /* Call application task */
    App_Thread( param );
}
#endif /* gHybridApp_d */
#endif

/*! *********************************************************************************
* \brief  This function represents the Application task.
*         This task reuses the stack allocated for the MainThread.
*         This function is called to process all events for the task. Events
*         include timers, messages and any other user defined events.
* \param[in]  argument
*
* \remarks  For bare-metal, process only one type of message at a time,
*           to allow other higher priority task to run.
*
********************************************************************************** */
void App_Thread (uint32_t param)
{
#if !defined(gHybridApp_d) || (!gHybridApp_d)
    osaEventFlags_t event = 0U;

    for (;;)
    {
        (void)OSA_EventWait(mAppEvent, osaEventFlagsAll_c, FALSE, mAppTaskWaitTime_c , &event);
#else
    {
#endif /* gHybridApp_d */
        /* Check for existing messages in queue */
        if (MSG_Pending(&mHostAppInputQueue))
        {
            /* Pointer for storing the messages from host. */
            appMsgFromHost_t *pMsgIn = MSG_DeQueue(&mHostAppInputQueue);

            if (pMsgIn != NULL)
            {
                /* Process it */
                App_HandleHostMessageInput(pMsgIn);

                /* Messages must always be freed. */
                (void)MSG_Free(pMsgIn);
            }
        }

        /* Check for existing messages in queue */
        if (MSG_Pending(&mAppCbInputQueue))
        {
            /* Pointer for storing the callback messages. */
            appMsgCallback_t *pMsgIn = MSG_DeQueue(&mAppCbInputQueue);

            if (pMsgIn != NULL)
            {
                /* Execute callback handler */
                if (pMsgIn->handler != NULL)
                {
                    pMsgIn->handler(pMsgIn->param);
                }

                /* Messages must always be freed. */
                (void)MSG_Free(pMsgIn);
            }
        }
#if !defined(gHybridApp_d) || (!gHybridApp_d)
        /* Signal the App_Thread again if there are more messages pending */
        event = MSG_Pending(&mHostAppInputQueue) ? gAppEvtMsgFromHostStack_c : 0U;
        event |= MSG_Pending(&mAppCbInputQueue) ? gAppEvtAppCallback_c : 0U;

        if (event != 0U)
        {
            (void)OSA_EventSet(mAppEvent, gAppEvtAppCallback_c);
        }

        /* For BareMetal break the while(1) after 1 run */
        if( gUseRtos_c == 0U )
        {
            break;
        }
#endif /* gHybridApp_d */
    }
}

/*
* board_specific_action_on_idle is declared weak.
* Its actual implementation is expected in borad.c
*/
WEAK void board_specific_action_on_idle(void)
{
}

#ifndef DUAL_MODE_APP
#if (IdleTaskAct_c & IdleTask_LowPower_c )
static void App_Idle(void)
{
#if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode)
    if( PWR_CheckIfDeviceCanGoToSleep() )
    {
#ifdef CPU_QN908X
#if (defined(BOARD_XTAL1_CLK_HZ) && (BOARD_XTAL1_CLK_HZ != CLK_XTAL_32KHZ))
#if (defined(CFG_CALIBRATION_ON_IDLE_TASK) && (CFG_CALIBRATION_ON_IDLE_TASK > 0))
        if(gRco32kCalibrationRequest)
        {
            /* perform RCO32K calibration */
            RCO32K_Calibrate();
            /* clear the calibration request flag */
            gRco32kCalibrationRequest = FALSE;
        }
#endif /* CFG_CALIBRATION_ON_IDLE_TASK */
#endif /* BOARD_XTAL1_CLK_HZ != CLK_XTAL_32KHZ */
#endif /* CPU_QN908X */

        /* Enter Low Power */
        (void)PWR_EnterLowPower();

#if gFSCI_IncludeLpmCommands_c
        /* Send Wake Up indication to FSCI */
        FSCI_SendWakeUpIndication();
#endif

#if gKBD_KeysCount_c > 0
#if !defined(CPU_JN518X)   // ON QN9090, PWR will directly notify the GPIO module for the source of IO wakeup
        /* Woke up on Keyboard Press */
        if(PWRLib_MCU_WakeupReason.Bits.FromKeyBoard != 0U)
        {
  #if defined(cPWR_EnableDeepSleepMode_8) && (cPWR_EnableDeepSleepMode_8)
            /* Skip over the key scan timer to improve low power consumption. */
            BleApp_HandleKeys(gKBD_EventPressPB1_c);
  #else
            KBD_SwitchPressedOnWakeUp();
  #endif
        }
#endif  /*  !defined(CPU_JN518X) */
#endif  /* gKBD_KeysCount_c */
    }
    else
    {
        /* Enter MCU Sleep */
        PWR_EnterSleep();
    }
#else

    /* check if any Radio calibration is needed, it requires asking for a dry run BLE sleep */
    if(XCVR_GetRecalDuration())
    {
        BLE_get_sleep_mode();
    }
#endif /* cPWR_UsePowerDownMode */
}
#endif /*  (IdleTaskAct_c & IdleTask_LowPower_c) */
#endif /* DUAL_MODE_APP */

#if (IdleTaskAct_c != 0)
/*
 * Treatment to be executed in Idle
 * - With an RTOS whether from the vApplicationIdleHook function or from a
 * dedicated task.
 * - in Bare metal mode  from the OSA Idle Task
 */
static void AppIdleCommon(void)
{
#if (gMaxBondedDevices_c > 0)
    uint16_t i;
    PDM_teStatus  pdmSt;
    OSA_MutexLock(bondingMutex, osaWaitForever_c);
    for (i=0; i<gMaxBondedDevices_c; i++)
    {
        if (bondEntries[i].nbRamWrite > 0)
        {
            uint32_t nextBleEventValue = BLE_TimeBeforeNextBleEvent();
            /* Avoid to block the LL during PDM save record
             * PDM_eSaveRecordData could take up to 8ms to save a 1 page record so we need to
             * make sure that no LL event is scheduled during a call to SaveRecordData
             */
            if (nextBleEventValue < 8000)
                break;
            APP_DBG_LOG("nextBleEventValue = %d",nextBleEventValue);
            APP_DBG_LOG("entry (%d) will be saved in PDM nbRamWrite = %d",i,bondEntries[i].nbRamWrite);
            bondEntries[i].nbRamWrite = 0;
            pdmSt = PDM_eSaveRecordData(bondEntries[i].pdmId,
                                                  &bondEntries[i],
                                                  sizeof(bleBondDeviceEntry));
            NOT_USED(pdmSt);
            assert(pdmSt == PDM_E_STATUS_OK);
            break;
        }
    }
    OSA_MutexUnlock(bondingMutex);
#endif
#if   (IdleTaskAct_c & IdleTask_BoardSpecific_c )
    board_specific_action_on_idle();
#endif
#if (IdleTaskAct_c & IdleTask_DualMode_c )
        dm_switch_IdleTask();
#elif (IdleTaskAct_c & IdleTask_LowPower_c )
    App_Idle();
#endif
}

#if (mAppIdleHook_c)
void vApplicationIdleHook(void)
{
    AppIdleCommon();
}

#else /* mAppIdleHook_c */
static void App_Idle_Task(osaTaskParam_t argument)
{
    for (;;)
    {
        AppIdleCommon();

        /* For BareMetal break the while(1) after 1 run */
        if (gUseRtos_c == 0U)
        {
            break;
        }
    }
}

#ifndef DUAL_MODE_APP
static osaStatus_t AppIdle_TaskInit(void)
#else
osaStatus_t AppIdle_TaskInit(void)
#endif
{
    osaStatus_t st;
    do {
        if(NULL != mAppIdleTaskId)
        {
            st = osaStatus_Error;
            break;
        }

    /* Task creation */
        mAppIdleTaskId = OSA_TaskCreate(OSA_TASK(App_Idle_Task), NULL);

        if( NULL == mAppIdleTaskId )
        {
            panic(0,0,0,0);
            st = osaStatus_Error;
            break;
        }
        st = osaStatus_Success;
    } while (0);
    return st;
}
#endif /* mAppIdleHook_c */
#endif /*IdleTaskAct_c != 0 */


bleResult_t App_Connect(
    gapConnectionRequestParameters_t*   pParameters,
    gapConnectionCallback_t             connCallback
)
{
    pfConnCallback = connCallback;

    return Gap_Connect(pParameters, App_ConnectionCallback);
}

bleResult_t App_StartAdvertising(
    gapAdvertisingCallback_t    advertisingCallback,
    gapConnectionCallback_t     connectionCallback
)
{
    pfAdvCallback = advertisingCallback;
    pfConnCallback = connectionCallback;

    return Gap_StartAdvertising(App_AdvertisingCallback, App_ConnectionCallback);
}

bleResult_t App_StartExtAdvertising(
    gapAdvertisingCallback_t    advertisingCallback,
    gapConnectionCallback_t     connectionCallback,
    uint8_t                     handle,
    uint16_t                    duration,
    uint8_t                     maxExtAdvEvents
)
{
    pfAdvCallback = advertisingCallback;
    pfConnCallback = connectionCallback;

    return Gap_StartExtAdvertising(App_AdvertisingCallback, App_ConnectionCallback, handle, duration, maxExtAdvEvents);
}

bleResult_t App_StartScanning(
    gapScanningParameters_t*    pScanningParameters,
    gapScanningCallback_t       scanningCallback,
    gapFilterDuplicates_t       enableFilterDuplicates,
    uint16_t                    duration,
    uint16_t                    period
)
{
    pfScanCallback = scanningCallback;

    return Gap_StartScanning(pScanningParameters, App_ScanningCallback,  enableFilterDuplicates, duration, period);
}

bleResult_t App_RegisterGattServerCallback(gattServerCallback_t  serverCallback)
{
    pfGattServerCallback = serverCallback;

    return GattServer_RegisterCallback(App_GattServerCallback);
}

bleResult_t App_RegisterGattClientProcedureCallback(gattClientProcedureCallback_t  callback)
{
    pfGattClientProcCallback = callback;

    return GattClient_RegisterProcedureCallback(App_GattClientProcedureCallback);
}

bleResult_t App_RegisterGattClientNotificationCallback(gattClientNotificationCallback_t  callback)
{
    pfGattClientNotifCallback = callback;

    return GattClient_RegisterNotificationCallback(App_GattClientNotificationCallback);
}

bleResult_t App_RegisterGattClientIndicationCallback(gattClientIndicationCallback_t  callback)
{
    pfGattClientIndCallback = callback;

    return GattClient_RegisterIndicationCallback(App_GattClientIndicationCallback);
}

bleResult_t App_RegisterLeCbCallbacks
(
    l2caLeCbDataCallback_t      pCallback,
    l2caLeCbControlCallback_t   pCtrlCallback
)
{
    pfL2caLeCbDataCallback = pCallback;
    pfL2caLeCbControlCallback = pCtrlCallback;

    return L2ca_RegisterLeCbCallbacks(App_L2caLeDataCallback, App_L2caLeControlCallback);
}

bleResult_t App_PostCallbackMessage
(
    appCallbackHandler_t   handler,
    appCallbackParam_t     param
)
{
    appMsgCallback_t *pMsgIn = NULL;

    /* Allocate a buffer with enough space to store the packet */
    pMsgIn = MSG_Alloc(sizeof (appMsgCallback_t));

    if (NULL == pMsgIn)
    {
        return gBleOutOfMemory_c;
    }

    pMsgIn->handler = handler;
    pMsgIn->param = param;

    /* Put message in the Cb App queue */
    (void)MSG_Queue(&mAppCbInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtAppCallback_c);

    return gBleSuccess_c;
}

void App_NvmInit(void)
{
#if (gMaxBondedDevices_c > 0)
    /* Init the bonded device list */
    uint16_t i = 0;
    PDM_teStatus  pdmSt;
    uint16_t pu16DataBytesRead = 0;
    FLib_MemSet(bondEntries, 0, sizeof(bondEntries));
    bondingMutex = OSA_MutexCreate();
    assert(bondingMutex != NULL);
    for (i=0; i<gMaxBondedDevices_c; i++)
    {
        bondEntries[i].pdmId = pdmId_BondEntry0+i;
        bondEntries[i].nbRamWrite = 0;
        /* Try to load the data from the PDM */
        if (PDM_bDoesDataExist(bondEntries[i].pdmId, &pu16DataBytesRead))
        {
            APP_DBG_LOG("Record = 0x%x loaded", bondEntries[i].pdmId);
            pdmSt = PDM_eReadDataFromRecord(bondEntries[i].pdmId, &bondEntries[i],
                                                         sizeof(bleBondDeviceEntry),
                                                         &pu16DataBytesRead);
            NOT_USED(pdmSt);
            assert(pdmSt == PDM_E_STATUS_OK);
            assert(sizeof(bleBondDeviceEntry) == pu16DataBytesRead);
        }
    }
#endif
}

void App_NvmErase(uint8_t mEntryIdx)
{
#if (gMaxBondedDevices_c > 0)
    APP_DBG_LOG("EntryIdx=%d", mEntryIdx);
    OSA_MutexLock(bondingMutex, osaWaitForever_c);
    if(mEntryIdx < (uint8_t)gMaxBondedDevices_c)
    {
        /* Check if a write is required */
        if (!FLib_MemCmpToVal(&bondEntries[mEntryIdx].aBondingHeader, 0, sizeof(bondEntries[mEntryIdx].aBondingHeader)) ||
            !FLib_MemCmpToVal(&bondEntries[mEntryIdx].aBondingDataDynamic, 0, sizeof(bondEntries[mEntryIdx].aBondingDataDynamic)) ||
            !FLib_MemCmpToVal(&bondEntries[mEntryIdx].aBondingDataStatic, 0, sizeof(bondEntries[mEntryIdx].aBondingDataStatic)) ||
            !FLib_MemCmpToVal(&bondEntries[mEntryIdx].aBondingDataDeviceInfo, 0, sizeof(bondEntries[mEntryIdx].aBondingDataDeviceInfo)) ||
            !FLib_MemCmpToVal(&bondEntries[mEntryIdx].aBondingDataDescriptor[0], 0, sizeof(bondEntries[mEntryIdx].aBondingDataDescriptor)))
        {
            FLib_MemSet(&bondEntries[mEntryIdx].aBondingHeader, 0, sizeof(bondEntries[mEntryIdx].aBondingHeader));
            FLib_MemSet(&bondEntries[mEntryIdx].aBondingDataDynamic, 0, sizeof(bondEntries[mEntryIdx].aBondingDataDynamic));
            FLib_MemSet(&bondEntries[mEntryIdx].aBondingDataStatic, 0, sizeof(bondEntries[mEntryIdx].aBondingDataStatic));
            FLib_MemSet(&bondEntries[mEntryIdx].aBondingDataDeviceInfo, 0, sizeof(bondEntries[mEntryIdx].aBondingDataDeviceInfo));
            FLib_MemSet(&bondEntries[mEntryIdx].aBondingDataDescriptor[0], 0, sizeof(bondEntries[mEntryIdx].aBondingDataDescriptor));
            bondEntries[mEntryIdx].nbRamWrite++;
        }
    }
    OSA_MutexUnlock(bondingMutex);
#else
    NOT_USED(mEntryIdx);
#endif
    APP_DBG_LOG("==> end");
}

void App_NvmWrite
(
    uint8_t  mEntryIdx,
    void*    pBondHeader,
    void*    pBondDataDynamic,
    void*    pBondDataStatic,
    void*    pBondDataDeviceInfo,
    void*    pBondDataDescriptor,
    uint8_t  mDescriptorIndex
)
{
#if (gMaxBondedDevices_c > 0)
    APP_DBG_LOG("EntryIdx=%d DescIdx=%d", mEntryIdx, mDescriptorIndex);
    OSA_MutexLock(bondingMutex, osaWaitForever_c);
    if(mEntryIdx < (uint8_t)gMaxBondedDevices_c)
    {
        if (pBondHeader != NULL)
        {
            FLib_MemCpy((void*)&bondEntries[mEntryIdx].aBondingHeader, pBondHeader, sizeof(bondEntries[mEntryIdx].aBondingHeader));
            bondEntries[mEntryIdx].nbRamWrite++;
            APP_DBG_LOG("pBondHeader");
        }

        if (pBondDataDynamic != NULL)
        {
            FLib_MemCpy((void*)&bondEntries[mEntryIdx].aBondingDataDynamic, pBondDataDynamic, sizeof(bondEntries[mEntryIdx].aBondingDataDynamic));
            bondEntries[mEntryIdx].nbRamWrite++;
            APP_DBG_LOG("pBondDataDynamic");
        }

        if (pBondDataStatic != NULL)
        {
            FLib_MemCpy((void*)&bondEntries[mEntryIdx].aBondingDataStatic, pBondDataStatic, sizeof(bondEntries[mEntryIdx].aBondingDataStatic));
            bondEntries[mEntryIdx].nbRamWrite++;
            APP_DBG_LOG("pBondDataStatic");
        }

        if (pBondDataDeviceInfo != NULL)
        {
            FLib_MemCpy((void*)&bondEntries[mEntryIdx].aBondingDataDeviceInfo, pBondDataDeviceInfo, sizeof(bondEntries[mEntryIdx].aBondingDataDeviceInfo));
            bondEntries[mEntryIdx].nbRamWrite++;
            APP_DBG_LOG("pBondDataDeviceInfo");
        }

        if (pBondDataDescriptor != NULL && mDescriptorIndex<gcGapMaximumSavedCccds_c)
        {
            FLib_MemCpy((void*)&bondEntries[mEntryIdx].aBondingDataDescriptor[mDescriptorIndex], pBondDataDescriptor, gBleBondDataDescriptorSize_c);
            bondEntries[mEntryIdx].nbRamWrite++;
            APP_DBG_LOG("pBondDataDescriptor");
        }
    }
    OSA_MutexUnlock(bondingMutex);
#else
    NOT_USED(mEntryIdx);
    NOT_USED(pBondHeader);
    NOT_USED(pBondDataDynamic);
    NOT_USED(pBondDataStatic);
    NOT_USED(pBondDataDeviceInfo);
    NOT_USED(pBondDataDescriptor);
    NOT_USED(mDescriptorIndex);
#endif
    APP_DBG_LOG("==> end");
}

void App_NvmRead
(
    uint8_t  mEntryIdx,
    void*    pBondHeader,
    void*    pBondDataDynamic,
    void*    pBondDataStatic,
    void*    pBondDataDeviceInfo,
    void*    pBondDataDescriptor,
    uint8_t  mDescriptorIndex
)
{
#if (gMaxBondedDevices_c > 0)
    APP_DBG_LOG("EntryIdx=%d DescIdx=%d", mEntryIdx, mDescriptorIndex);
    OSA_MutexLock(bondingMutex, osaWaitForever_c);
    if(mEntryIdx < (uint8_t)gMaxBondedDevices_c)
    {
        if (pBondHeader != NULL)
        {
            FLib_MemCpy(pBondHeader, (void*)&bondEntries[mEntryIdx].aBondingHeader, sizeof(bondEntries[mEntryIdx].aBondingHeader));
        }

        if (pBondDataDynamic != NULL)
        {
            FLib_MemCpy(pBondDataDynamic, (void*)&bondEntries[mEntryIdx].aBondingDataDynamic, sizeof(bondEntries[mEntryIdx].aBondingDataDynamic));
        }

        if (pBondDataStatic != NULL)
        {
            FLib_MemCpy(pBondDataStatic, (void*)&bondEntries[mEntryIdx].aBondingDataStatic, sizeof(bondEntries[mEntryIdx].aBondingDataStatic));

        }

        if (pBondDataDeviceInfo != NULL)
        {
            FLib_MemCpy(pBondDataDeviceInfo, (void*)&bondEntries[mEntryIdx].aBondingDataDeviceInfo, sizeof(bondEntries[mEntryIdx].aBondingDataDeviceInfo));

        }

        if (pBondDataDescriptor != NULL && mDescriptorIndex<gcGapMaximumSavedCccds_c)
        {
            FLib_MemCpy(pBondDataDescriptor, (void*)&bondEntries[mEntryIdx].aBondingDataDescriptor[mDescriptorIndex], gBleBondDataDescriptorSize_c);
        }
    }
    OSA_MutexUnlock(bondingMutex);
#else
    NOT_USED(mEntryIdx);
    NOT_USED(pBondHeader);
    NOT_USED(pBondDataDynamic);
    NOT_USED(pBondDataStatic);
    NOT_USED(pBondDataDeviceInfo);
    NOT_USED(pBondDataDescriptor);
    NOT_USED(mDescriptorIndex);
#endif
    APP_DBG_LOG("==> end");
}

#if !defined(MULTICORE_CONNECTIVITY_CORE) || (!MULTICORE_CONNECTIVITY_CORE)
void App_GenericCallback (gapGenericEvent_t* pGenericEvent)
{
    appMsgFromHost_t *pMsgIn = NULL;

    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(gapGenericEvent_t));

    if (NULL != pMsgIn)
    {
        pMsgIn->msgType = gAppGapGenericMsg_c;
        FLib_MemCpy(&pMsgIn->msgData.genericMsg, pGenericEvent, sizeof(gapGenericEvent_t));
        /* Put message in the Host Stack to App queue */
        (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);
        /* Signal application */
        (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
    }
}
#endif /* MULTICORE_CONNECTIVITY_CORE */

/************************************************************************************
*************************************************************************************
* Private functions
*************************************************************************************
************************************************************************************/

/*****************************************************************************
* Handles all key events for this device.
* Interface assumptions: None
* Return value: None
*****************************************************************************/
#if gKeyBoardSupported_d && (gKBD_KeysCount_c > 0) && !defined (DUAL_MODE_APP)
static void App_KeyboardCallBack
  (
  uint8_t events  /*IN: Events from keyboard module  */
  )
{
    BleApp_HandleKeys(events);
}
#endif

/*****************************************************************************
* Handles all messages received from the host task.
* Interface assumptions: None
* Return value: None
*****************************************************************************/
static void App_HandleHostMessageInput(appMsgFromHost_t* pMsg)
{
    switch ( pMsg->msgType )
    {
        case gAppGapGenericMsg_c:
        {
            if (pfGenericCallback != NULL)
            {
                pfGenericCallback(&pMsg->msgData.genericMsg);
            }
            else
            {
                BleApp_GenericCallback(&pMsg->msgData.genericMsg);
            }
            break;
        }
        case gAppGapAdvertisementMsg_c:
        {
            if (pfAdvCallback != NULL)
            {
                pfAdvCallback(&pMsg->msgData.advMsg);
            }
            break;
        }
        case gAppGapScanMsg_c:
        {
            if (pfScanCallback != NULL)
            {
                pfScanCallback(&pMsg->msgData.scanMsg);
            }
            break;
        }
        case gAppGapConnectionMsg_c:
        {
            if (pfConnCallback != NULL)
            {
                pfConnCallback(pMsg->msgData.connMsg.deviceId, &pMsg->msgData.connMsg.connEvent);
            }
            break;
        }
        case gAppGattServerMsg_c:
        {
            if (pfGattServerCallback != NULL)
            {
                pfGattServerCallback(pMsg->msgData.gattServerMsg.deviceId, &pMsg->msgData.gattServerMsg.serverEvent);
            }
            break;
        }
        case gAppGattClientProcedureMsg_c:
        {
            if (pfGattClientProcCallback != NULL)
            {
                pfGattClientProcCallback(
                    pMsg->msgData.gattClientProcMsg.deviceId,
                    pMsg->msgData.gattClientProcMsg.procedureType,
                    pMsg->msgData.gattClientProcMsg.procedureResult,
                    pMsg->msgData.gattClientProcMsg.error);
            }
            break;
        }
        case gAppGattClientNotificationMsg_c:
        {
            if (pfGattClientNotifCallback != NULL)
            {
                pfGattClientNotifCallback(
                    pMsg->msgData.gattClientNotifIndMsg.deviceId,
                    pMsg->msgData.gattClientNotifIndMsg.characteristicValueHandle,
                    pMsg->msgData.gattClientNotifIndMsg.aValue,
                    pMsg->msgData.gattClientNotifIndMsg.valueLength);
            }
            break;
        }
        case gAppGattClientIndicationMsg_c:
        {
            if (pfGattClientIndCallback != NULL)
            {
                pfGattClientIndCallback(
                    pMsg->msgData.gattClientNotifIndMsg.deviceId,
                    pMsg->msgData.gattClientNotifIndMsg.characteristicValueHandle,
                    pMsg->msgData.gattClientNotifIndMsg.aValue,
                    pMsg->msgData.gattClientNotifIndMsg.valueLength);
            }
            break;
        }
        case gAppL2caLeDataMsg_c:
        {
            if (pfL2caLeCbDataCallback != NULL)
            {
                pfL2caLeCbDataCallback(
                    pMsg->msgData.l2caLeCbDataMsg.deviceId,
                    pMsg->msgData.l2caLeCbDataMsg.channelId,
                    pMsg->msgData.l2caLeCbDataMsg.aPacket,
                    pMsg->msgData.l2caLeCbDataMsg.packetLength);
            }
            break;
        }
        case gAppL2caLeControlMsg_c:
        {
            if (pfL2caLeCbControlCallback != NULL)
            {
                pfL2caLeCbControlCallback(&pMsg->msgData.l2caLeCbControlMsg);
            }
            break;
        }
#if !(defined(FSL_FEATURE_SOC_CAU3_COUNT) && (FSL_FEATURE_SOC_CAU3_COUNT > 0))
        case gAppSecLibMultiplyMsg_c:
        {
            computeDhKeyParam_t *pData = pMsg->msgData.secLibMsgData.pData;
            bool_t ready = SecLib_HandleMultiplyStep(pData);

            if(ready == FALSE)
            {
                SecLib_ExecMultiplicationCb(pData);
            }
            else
            {
                bleResult_t status;

                status = Gap_ResumeLeScStateMachine(pData);
                if (status != gBleSuccess_c)
                {
                    /* Not enough memory to resume LE SC operations */
                    panic(0, (uint32_t)Gap_ResumeLeScStateMachine, 0, 0);
                }
            }

            break;
        }
#endif
        default:
        {
            ; /* No action required */
            break;
        }
    }
}



MULTICORE_STATIC void App_ConnectionCallback (deviceId_t peerDeviceId, gapConnectionEvent_t* pConnectionEvent)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) || (gFsciBleTest_d == 1))
    fsciBleGapConnectionEvtMonitor(peerDeviceId, pConnectionEvent);
#else
    appMsgFromHost_t *pMsgIn = NULL;
    connectionMsg_t *pConnMsg = NULL;        /* used to get relative address of connEvent member */
    gapConnectionEvent_t *pConnEvent = NULL; /* used to get relative address of eventData member */

    uint32_t msgLen = (uint32_t)&(pMsgIn->msgData) + sizeof(connectionMsg_t);

    if(pConnectionEvent->eventType == gConnEvtKeysReceived_c)
    {
        gapSmpKeys_t    *pKeys = pConnectionEvent->eventData.keysReceivedEvent.pKeys;

        /* Take into account alignment */
        msgLen = (uint32_t)&(pMsgIn->msgData) + (uint32_t)&(pConnMsg->connEvent) +
                 (uint32_t)&(pConnEvent->eventData) + sizeof(gapKeysReceivedEvent_t) + sizeof(gapSmpKeys_t);

        if (pKeys->aLtk != NULL)
        {
            msgLen += 2U * sizeof(uint8_t) + (uint32_t)pKeys->cLtkSize + (uint32_t)pKeys->cRandSize;
        }

        msgLen += (pKeys->aIrk != NULL) ? (gcSmpIrkSize_c + gcBleDeviceAddressSize_c) : 0U;
        msgLen += (pKeys->aCsrk != NULL) ? gcSmpCsrkSize_c : 0U;
    }

    pMsgIn = MSG_Alloc(msgLen);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGapConnectionMsg_c;
    pMsgIn->msgData.connMsg.deviceId = peerDeviceId;

    if(pConnectionEvent->eventType == gConnEvtKeysReceived_c)
    {
        union
        {
            uint8_t      *pu8;
            gapSmpKeys_t *pObject;
        } temp; /* MISRA rule 11.3 */

        gapSmpKeys_t    *pKeys = pConnectionEvent->eventData.keysReceivedEvent.pKeys;
        uint8_t         *pCursor = (uint8_t*)&pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys;

        pMsgIn->msgData.connMsg.connEvent.eventType = gConnEvtKeysReceived_c;
        pCursor += sizeof(void*);

        temp.pu8 = pCursor;
        pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys = temp.pObject;

        /* Copy SMP Keys structure */
        FLib_MemCpy(pCursor, pConnectionEvent->eventData.keysReceivedEvent.pKeys, sizeof(gapSmpKeys_t));
        pCursor += sizeof(gapSmpKeys_t);

        if (pKeys->aLtk != NULL)
        {
            /* Copy LTK */
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->cLtkSize = pKeys->cLtkSize;
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->aLtk = pCursor;
            FLib_MemCpy(pCursor, pKeys->aLtk, pKeys->cLtkSize);
            pCursor += pKeys->cLtkSize;

            /* Copy RAND */
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->cRandSize = pKeys->cRandSize;
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->aRand = pCursor;
            FLib_MemCpy(pCursor, pKeys->aRand, pKeys->cRandSize);
            pCursor += pKeys->cRandSize;
        }

        if (pKeys->aIrk != NULL)
        {
            /* Copy IRK */
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->aIrk = pCursor;
            FLib_MemCpy(pCursor, pKeys->aIrk, gcSmpIrkSize_c);
            pCursor += gcSmpIrkSize_c;

            /* Copy Address*/
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->addressType = pKeys->addressType;
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->aAddress = pCursor;
            FLib_MemCpy(pCursor, pKeys->aAddress, gcBleDeviceAddressSize_c);
            pCursor += gcBleDeviceAddressSize_c;
        }

        if (pKeys->aCsrk != NULL)
        {
            /* Copy CSRK */
            pMsgIn->msgData.connMsg.connEvent.eventData.keysReceivedEvent.pKeys->aCsrk = pCursor;
            FLib_MemCpy(pCursor, pKeys->aCsrk, gcSmpCsrkSize_c);
        }
    }
    else
    {
        FLib_MemCpy(&pMsgIn->msgData.connMsg.connEvent, pConnectionEvent, sizeof(gapConnectionEvent_t));
    }

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}


MULTICORE_STATIC void App_AdvertisingCallback (gapAdvertisingEvent_t* pAdvertisingEvent)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) || (gFsciBleTest_d == 1))
    fsciBleGapAdvertisingEvtMonitor(pAdvertisingEvent);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(gapAdvertisingEvent_t));

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGapAdvertisementMsg_c;
    pMsgIn->msgData.advMsg.eventType = pAdvertisingEvent->eventType;
    pMsgIn->msgData.advMsg.eventData = pAdvertisingEvent->eventData;

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_ScanningCallback (gapScanningEvent_t* pScanningEvent)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) || (gFsciBleTest_d == 1))
    fsciBleGapScanningEvtMonitor(pScanningEvent);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    uint32_t msgLen = (uint32_t)&(pMsgIn->msgData) + sizeof(gapScanningEvent_t);

    if (pScanningEvent->eventType == gDeviceScanned_c)
    {
        msgLen += pScanningEvent->eventData.scannedDevice.dataLength;
    }
    else if (pScanningEvent->eventType == gExtDeviceScanned_c)
    {
        msgLen += pScanningEvent->eventData.extScannedDevice.dataLength;
    }
    else
    {
        /* msgLen does not modify for all other event types */
    }

    pMsgIn = MSG_Alloc(msgLen);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGapScanMsg_c;
    pMsgIn->msgData.scanMsg.eventType = pScanningEvent->eventType;

    if (pScanningEvent->eventType == gScanCommandFailed_c)
    {
        pMsgIn->msgData.scanMsg.eventData.failReason = pScanningEvent->eventData.failReason;
    }
    else if (pScanningEvent->eventType == gDeviceScanned_c)
    {
        FLib_MemCpy(&pMsgIn->msgData.scanMsg.eventData.scannedDevice,
                    &pScanningEvent->eventData.scannedDevice,
                    sizeof(pScanningEvent->eventData.scannedDevice));

        /* Copy data after the gapScanningEvent_t structure and update the data pointer*/
        pMsgIn->msgData.scanMsg.eventData.scannedDevice.data = (uint8_t*)&pMsgIn->msgData + sizeof(gapScanningEvent_t);
        FLib_MemCpy(pMsgIn->msgData.scanMsg.eventData.scannedDevice.data,
                    pScanningEvent->eventData.scannedDevice.data,
                    pScanningEvent->eventData.scannedDevice.dataLength);
    }
    else if (pScanningEvent->eventType == gExtDeviceScanned_c)
    {
        FLib_MemCpy(&pMsgIn->msgData.scanMsg.eventData.extScannedDevice,
                    &pScanningEvent->eventData.extScannedDevice,
                    sizeof(pScanningEvent->eventData.extScannedDevice));

        /* Copy data after the gapScanningEvent_t structure and update the data pointer*/
        pMsgIn->msgData.scanMsg.eventData.extScannedDevice.pData = (uint8_t*)&pMsgIn->msgData + sizeof(gapScanningEvent_t);
        FLib_MemCpy(pMsgIn->msgData.scanMsg.eventData.extScannedDevice.pData,
                    pScanningEvent->eventData.extScannedDevice.pData,
                    pScanningEvent->eventData.extScannedDevice.dataLength);
    }
    else
    {
        /* no action for all other event types */
    }

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_GattServerCallback
(
    deviceId_t          peerDeviceId,
    gattServerEvent_t*  pServerEvent
)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleGattServerEvtMonitor(peerDeviceId, pServerEvent);
#else
    appMsgFromHost_t *pMsgIn = NULL;
    uint32_t msgLen = (uint32_t)&(pMsgIn->msgData) + sizeof(gattServerMsg_t);

    if (pServerEvent->eventType == gEvtAttributeWritten_c ||
        pServerEvent->eventType == gEvtAttributeWrittenWithoutResponse_c)
    {
        msgLen += pServerEvent->eventData.attributeWrittenEvent.cValueLength;
    }

    pMsgIn = MSG_Alloc(msgLen);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGattServerMsg_c;
    pMsgIn->msgData.gattServerMsg.deviceId = peerDeviceId;
    FLib_MemCpy(&pMsgIn->msgData.gattServerMsg.serverEvent, pServerEvent, sizeof(gattServerEvent_t));

    if ((pMsgIn->msgData.gattServerMsg.serverEvent.eventType == gEvtAttributeWritten_c) ||
        (pMsgIn->msgData.gattServerMsg.serverEvent.eventType == gEvtAttributeWrittenWithoutResponse_c))
    {
        /* Copy value after the gattServerEvent_t structure and update the aValue pointer*/
        pMsgIn->msgData.gattServerMsg.serverEvent.eventData.attributeWrittenEvent.aValue =
          (uint8_t *)&pMsgIn->msgData.gattServerMsg.serverEvent.eventData.attributeWrittenEvent.aValue + sizeof(uint8_t*);
        FLib_MemCpy(pMsgIn->msgData.gattServerMsg.serverEvent.eventData.attributeWrittenEvent.aValue,
                    pServerEvent->eventData.attributeWrittenEvent.aValue,
                    pServerEvent->eventData.attributeWrittenEvent.cValueLength);

    }

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_GattClientProcedureCallback
(
    deviceId_t              deviceId,
    gattProcedureType_t     procedureType,
    gattProcedureResult_t   procedureResult,
    bleResult_t             error)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleGattClientProcedureEvtMonitor(deviceId, procedureType, procedureResult, error);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(gattClientProcMsg_t));

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGattClientProcedureMsg_c;
    pMsgIn->msgData.gattClientProcMsg.deviceId = deviceId;
    pMsgIn->msgData.gattClientProcMsg.procedureType = procedureType;
    pMsgIn->msgData.gattClientProcMsg.error = error;
    pMsgIn->msgData.gattClientProcMsg.procedureResult = procedureResult;

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_GattClientNotificationCallback
(
    deviceId_t      deviceId,
    uint16_t        characteristicValueHandle,
    uint8_t*        aValue,
    uint16_t        valueLength
)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleGattClientNotificationEvtMonitor(deviceId, characteristicValueHandle, aValue, valueLength);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    /* Allocate a buffer with enough space to store also the notified value*/
    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(gattClientNotifIndMsg_t)
                        + (uint32_t)valueLength);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGattClientNotificationMsg_c;
    pMsgIn->msgData.gattClientNotifIndMsg.deviceId = deviceId;
    pMsgIn->msgData.gattClientNotifIndMsg.characteristicValueHandle = characteristicValueHandle;
    pMsgIn->msgData.gattClientNotifIndMsg.valueLength = valueLength;

    /* Copy value after the gattClientNotifIndMsg_t structure and update the aValue pointer*/
    pMsgIn->msgData.gattClientNotifIndMsg.aValue = (uint8_t*)&pMsgIn->msgData + sizeof(gattClientNotifIndMsg_t);
    FLib_MemCpy(pMsgIn->msgData.gattClientNotifIndMsg.aValue, aValue, valueLength);

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_GattClientIndicationCallback
(
    deviceId_t      deviceId,
    uint16_t        characteristicValueHandle,
    uint8_t*        aValue,
    uint16_t        valueLength
)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleGattClientIndicationEvtMonitor(deviceId, characteristicValueHandle, aValue, valueLength);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    /* Allocate a buffer with enough space to store also the notified value*/
    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(gattClientNotifIndMsg_t)
                        + (uint32_t)valueLength);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppGattClientIndicationMsg_c;
    pMsgIn->msgData.gattClientNotifIndMsg.deviceId = deviceId;
    pMsgIn->msgData.gattClientNotifIndMsg.characteristicValueHandle = characteristicValueHandle;
    pMsgIn->msgData.gattClientNotifIndMsg.valueLength = valueLength;

    /* Copy value after the gattClientIndIndMsg_t structure and update the aValue pointer*/
    pMsgIn->msgData.gattClientNotifIndMsg.aValue = (uint8_t*)&pMsgIn->msgData + sizeof(gattClientNotifIndMsg_t);
    FLib_MemCpy(pMsgIn->msgData.gattClientNotifIndMsg.aValue, aValue, valueLength);

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_L2caLeDataCallback
(
    deviceId_t deviceId,
    uint16_t channelId,
    uint8_t* pPacket,
    uint16_t packetLength
)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleL2capCbLeCbDataEvtMonitor(deviceId, channelId, pPacket, packetLength);
#else
    appMsgFromHost_t *pMsgIn = NULL;

    /* Allocate a buffer with enough space to store the packet */
    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + (sizeof(l2caLeCbDataMsg_t) - 1U)
                        + (uint32_t)packetLength);

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppL2caLeDataMsg_c;
    pMsgIn->msgData.l2caLeCbDataMsg.deviceId = deviceId;
    pMsgIn->msgData.l2caLeCbDataMsg.channelId = channelId;
    pMsgIn->msgData.l2caLeCbDataMsg.packetLength = packetLength;

    FLib_MemCpy(pMsgIn->msgData.l2caLeCbDataMsg.aPacket, pPacket, packetLength);

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_L2caLeControlCallback
(
    l2capControlMessage_t* pMessage
)
{
#if defined(MULTICORE_APPLICATION_CORE) && (MULTICORE_APPLICATION_CORE == 1) && ((gFsciBleBBox_d == 1) )
    fsciBleL2capCbLeCbControlEvtMonitor(pMessage);
#else
    appMsgFromHost_t *pMsgIn = NULL;
    uint8_t messageLength = 0U;

    switch (pMessage->messageType) {
        case gL2ca_LePsmConnectRequest_c:
        {
            messageLength = sizeof(l2caLeCbConnectionRequest_t);
            break;
        }
        case gL2ca_LePsmConnectionComplete_c:
        {
            messageLength = sizeof(l2caLeCbConnectionComplete_t);
            break;
        }
        case gL2ca_LePsmDisconnectNotification_c:
        {
            messageLength = sizeof(l2caLeCbDisconnection_t);
            break;
        }
        case gL2ca_NoPeerCredits_c:
        {
            messageLength = sizeof(l2caLeCbNoPeerCredits_t);
            break;
        }
        case gL2ca_LocalCreditsNotification_c:
        {
            messageLength = sizeof(l2caLeCbLocalCreditsNotification_t);
            break;
        }
        case gL2ca_Error_c:
        {
            messageLength = sizeof(l2caLeCbError_t);
            break;
        }
        default:
        {
            messageLength = 0U;
            break;
        }
    }

    if (messageLength == 0U)
    {
        return;
    }

    /* Allocate a buffer with enough space to store the biggest packet */
    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(l2capControlMessage_t));

    if (NULL == pMsgIn)
    {
          return;
    }

    pMsgIn->msgType = gAppL2caLeControlMsg_c;
    pMsgIn->msgData.l2caLeCbControlMsg.messageType = pMessage->messageType;

    FLib_MemCpy(&pMsgIn->msgData.l2caLeCbControlMsg.messageData, &pMessage->messageData, messageLength);

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
#endif /* (MULTICORE_APPLICATION_CORE == 1) && (gFsciBleBBox_d == 1) */
}

MULTICORE_STATIC void App_SecLibMultCallback
(
    computeDhKeyParam_t *pData
)
{
    appMsgFromHost_t *pMsgIn = NULL;

    /* Allocate a buffer with enough space to store also the notified value*/
    pMsgIn = MSG_Alloc((uint32_t)&(pMsgIn->msgData) + sizeof(secLibMsgData_t));

    if (NULL == pMsgIn)
    {
        return;
    }

    pMsgIn->msgType = gAppSecLibMultiplyMsg_c;
    pMsgIn->msgData.secLibMsgData.pData = pData;

    /* Put message in the Host Stack to App queue */
    (void)MSG_Queue(&mHostAppInputQueue, pMsgIn);

    /* Signal application */
    (void)OSA_EventSet(mAppEvent, gAppEvtMsgFromHostStack_c);
}
#if !defined (SOTA_ENABLED) && !defined (DUAL_MODE_APP)
#if !defined(gUseHciTransportDownward_d) || (!gUseHciTransportDownward_d)
/* Called by BLE when a connect is received */
static void BLE_SignalFromISRCallback(void)
{
#if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode)
    PWR_DisallowDeviceToSleep();
#endif /* cPWR_UsePowerDownMode */
}
#endif /* !gUseHciTransportDownward_d */


/*! *********************************************************************************
* \brief  This function will try to put the MCU into a deep sleep mode for at most
*         the maximum OS idle time specified. Else the MCU will enter a sleep mode
*         until the first IRQ.
*
* \param[in]  xExpectedIdleTime  The idle time in OS ticks
*
* \remarks  This feature is available only for FreeRTOS.
*           This function will replace the default implementation from
*           fsl_tickless_systick.c which is defined as weak.
*
********************************************************************************** */
#ifndef CPU_JN518X
#if defined(cPWR_UsePowerDownMode) && (cPWR_UsePowerDownMode) && (configUSE_TICKLESS_IDLE != 0)
void vPortSuppressTicksAndSleep( TickType_t xExpectedIdleTime )
{
    PWRLib_WakeupReason_t wakeupReason;

    if( PWR_CheckIfDeviceCanGoToSleep() && (xExpectedIdleTime > mAppOverheadTicks_c))
    {
        /* Set deep-sleep duration. Compensate for the application processing overhead time */
        PWR_SetDeepSleepTimeInMs((xExpectedIdleTime - mAppOverheadTicks_c) * portTICK_PERIOD_MS);
        PWR_ResetTotalSleepDuration();

        /* Enter Low Power */
        wakeupReason = PWR_EnterLowPower();

#if gKBD_KeysCount_c > 0
        /* Woke up on Keyboard Press */
        if(wakeupReason.Bits.FromKeyBoard != 0)
        {
            KBD_SwitchPressedOnWakeUp();
        }
#endif
        /* Get actual deep sleep time, and converted to OS ticks */
        xExpectedIdleTime = PWR_GetTotalSleepDurationMS() / portTICK_PERIOD_MS;

        portENTER_CRITICAL();
        /* Update the OS time ticks. */
        vTaskStepTick( xExpectedIdleTime );
        portEXIT_CRITICAL();
    }
    else
    {
        /* Enter MCU Sleep */
        PWR_EnterSleep();
    }
}
#endif
#else   /* CPU_JN518X */
#if defined(cPWR_FullPowerDownMode) && (cPWR_FullPowerDownMode) && (configUSE_TICKLESS_IDLE != 0)
void vPortSuppressTicksAndSleep( TickType_t xExpectedIdleTime )
{
    uint32_t tmrMgrExpiryTimeMs = ~0UL;
    uint32_t sleep_duration_ms = 0;
    PWR_WakeupReason_t wakeupReason;

#if defined gTimerMgrUseLpcRtc_c && (gTimerMgrUseLpcRtc_c > 0)
    /* remember if we had to reprogram the RTC IRQ to generate the RTOS Tick */
    bool next_irq_not_for_TMR = false;
#endif

    if (PWR_CheckIfDeviceCanGoToSleep())
    {
        /* Can only enter here if cPWR_FullPowerDownMode is configured */
        xExpectedIdleTime -= mAppOverheadTicks_c;
        if (xExpectedIdleTime > 0)
        {
            uint32_t time_ms = xExpectedIdleTime * portTICK_PERIOD_MS;
            assert(time_ms <= 0xffff);

#if defined gTimerMgrUseLpcRtc_c && (gTimerMgrUseLpcRtc_c > 0)
            //TMR_MGR: Get next timer manager expiry time if any
            if (RTC->CTRL & RTC_CTRL_RTC1KHZ_EN_MASK)
                tmrMgrExpiryTimeMs = RTC_GetWakeupCount(RTC);
#endif
            uint32_t initial_count;
            /* TMR_MGR: Update RTC Threshold only if RTOS needs to wakeup earlier */
            if(time_ms < tmrMgrExpiryTimeMs || tmrMgrExpiryTimeMs == ~0UL)
            {
#if defined gTimerMgrUseLpcRtc_c && (gTimerMgrUseLpcRtc_c > 0)
                 next_irq_not_for_TMR = true;
#endif
                PWR_SetDeepSleepTimeInMs(time_ms);
                initial_count = time_ms;
            }
            else
            {
                initial_count = tmrMgrExpiryTimeMs;
            }
            PWR_ResetTotalSleepDuration(initial_count);
        }

        wakeupReason = PWR_EnterLowPower();
        PWR_DBG_LOG("wakeReason=%x", (uint16_t)wakeupReason.AllBits);
        if (wakeupReason.AllBits != 0UL)
        {
            sleep_duration_ms = PWR_GetTotalSleepDurationMs();
            xExpectedIdleTime = sleep_duration_ms / portTICK_PERIOD_MS;

#if defined gTimerMgrUseLpcRtc_c && (gTimerMgrUseLpcRtc_c > 0)
            if (next_irq_not_for_TMR)
            {
                RTC_ClearStatusFlags(RTC, kRTC_WakeupFlag);
                NVIC_ClearPendingIRQ(RTC_IRQn);
            }
            if (tmrMgrExpiryTimeMs != ~0UL)
            {
                tmrMgrExpiryTimeMs -= sleep_duration_ms;
                PWR_DBG_LOG("nextDelayMs=%d ", tmrMgrExpiryTimeMs);
                RTC_SetWakeupCount(RTC, (uint16_t)tmrMgrExpiryTimeMs);
            }
#endif

            // Fix: ticks = time in mS asleep / mS per each tick (portTICK_PERIOD_MS)

            /* Restart SysTick so it runs from portNVIC_SYSTICK_LOAD_REG
            again, then set portNVIC_SYSTICK_LOAD_REG back to its standard
            value. The critical section is used to ensure the tick interrupt
            can only execute once in the case that the reload register is near
            zero. */
            /* Systick->VAL and LOAD : already restored by Restore_CM4_registers*/
            portENTER_CRITICAL();
            SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; /* Systick restart now */
            /* Update the OS time ticks. */
            vTaskStepTick( xExpectedIdleTime );
            //SysTick->LOAD = (ulTimerCountsForOneTick - 1UL)  & SysTick_LOAD_RELOAD_Msk;
            portEXIT_CRITICAL();
        }
        else
        {
            /* Didn't actually sleep, some activity pevented it  */
#if defined gTimerMgrUseLpcRtc_c && (gTimerMgrUseLpcRtc_c > 0)
            if (tmrMgrExpiryTimeMs != ~0UL)
            {
                sleep_duration_ms = PWR_GetTotalSleepDurationMs();
                tmrMgrExpiryTimeMs -= sleep_duration_ms;
                PWR_DBG_LOG("nextDelayMs2=%d ", tmrMgrExpiryTimeMs);
                RTC_SetWakeupCount(RTC, (uint16_t)tmrMgrExpiryTimeMs);
            }
#endif
        }
    }  /* PWR_CheckIfDeviceCanGoToSleep */
    else
    {
        /* We already know we can't power down: Enter MCU Sleep */
        PWR_EnterSleep();
    }

}
#endif /*cPWR_FullPowerDownMode */
#endif  /* CPU_JN518X */


#endif /* SOTA_ENABLED */

void vApplicationStackOverflowHook( void * xTask, char *pcTaskName )
{
    PRINTF("Stack Overflow detected in %s\r\n", pcTaskName);
    while (1);
}
