/*! *********************************************************************************
 * \addtogroup BLE
 * @{
 ********************************************************************************** */
/*! *********************************************************************************
* Copyright (c) 2015, Freescale Semiconductor, Inc.
* Copyright 2016-2018 NXP
* All rights reserved.
*
* \file
*
* SPDX-License-Identifier: BSD-3-Clause
********************************************************************************** */

#ifndef BLE_INIT_H
#define BLE_INIT_H

/************************************************************************************
*************************************************************************************
* Includes
*************************************************************************************
************************************************************************************/
#include "ble_general.h"

/************************************************************************************
*************************************************************************************
* Public macros - Do not modify directly! Override in app_preinclude.h if needed.
*************************************************************************************
************************************************************************************/
#ifndef gBleXcvrInitRetryCount_c
#define gBleXcvrInitRetryCount_c (10U)
#endif

/************************************************************************************
*************************************************************************************
* Public functions
*************************************************************************************
************************************************************************************/

/************************************************************************************
*************************************************************************************
* Private memory declarations
*************************************************************************************
************************************************************************************/
#if !defined (CPU_QN908X) && !defined (CPU_JN518X)
extern bool_t gEnableSingleAdvertisement;
extern bool_t gMCUSleepDuringBleEvents;
#endif /* CPU_QN908X */

/*! *********************************************************************************
* \brief  Performs full initialization of the BLE stack.
*
* \param[in] genericCallback  Callback used by the Host Stack to propagate GAP generic
* events to the application.
*
* \return  gBleSuccess_c or error.
*
* \remarks The gInitializationComplete_c generic GAP event is triggered on completion.
*
********************************************************************************** */
bleResult_t Ble_Initialize
(
    gapGenericCallback_t gapGenericCallback
);

#endif /* BLE_INIT_H */

/*! *********************************************************************************
* @}
********************************************************************************** */
